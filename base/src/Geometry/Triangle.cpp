
#include "Geometry/Triangle.h"

// STL includes
#include <queue>

// ArcaWave include
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
Triangle<T, V>::Triangle() :
    Vertices({Matrix<T, V, 1>::Zero(), Matrix<T, V, 1>::Zero(), Matrix<T, V, 1>::Zero()}),
    Normals({Matrix<T, V, 1>::Zero(), Matrix<T, V, 1>::Zero(), Matrix<T, V, 1>::Zero()})
{}

template<typename T, uint_fast32_t V>
Triangle<T, V>::Triangle(const Triad<T, V>& Vertices) :
    Vertices(Vertices)
{
    Matrix<T, V, 1> n = Normal();
    Normals = { n, n, n };
}

template<typename T, uint_fast32_t V>
Triangle<T, V>::Triangle(const Triad<T, V>& Vertices, const Triad<T, V>& Normals) :
    Vertices(Vertices), Normals(Normals)
{}

template<typename T, uint_fast32_t V>
Triangle<T, V>::~Triangle()
{}

template<typename T, uint_fast32_t V>
Matrix<T, V, 1> Triangle<T, V>::Barycentric(const Matrix<T, V, 1>& Point) const
{
    T triArea = Area();
    return Matrix<T, V, 1>
    (
        Area({Vertices.B, Vertices.C, Point})/triArea, 
        Area({Vertices.C, Vertices.A, Point})/triArea, 
        Area({Vertices.A, Vertices.B, Point})/triArea
    );
}

template<typename T, uint_fast32_t V>
Matrix<T, V, 1> Triangle<T, V>::Center() const
{
    return (Vertices.A + Vertices.B + Vertices.C)/3;
}

template<typename T, uint_fast32_t V>
T Triangle<T, V>::Intersects(const Ray<T, V>& R) const
{
    // Checks if the ray is parallel to the triangle, and if it is, return infinity
    Matrix<T, V, 1> n = Normal();
    T d = R.Direction.dot(n);
    if(fabs(d) <= FLOATING_POINT_ERROR<T>)
        return numeric_limits<T>::infinity();

    // Finds the distance along the ray befire it intersects with the triangle's plane
    T t = (Vertices.A - R.Origin).dot(n) / d;

    // As long as the triangle is in front of the ray and the ray intersects directly with the triangle, return the distance
    if(t > 0 && On(R.Project(t)))
        return t;

    // Otherwise return infinity
    return numeric_limits<T>::infinity();
}

template<typename T, uint_fast32_t V>
Matrix<T, V, 1> Triangle<T, V>::Normal() const
{
    return (Vertices.B - Vertices.A).cross(Vertices.C - Vertices.A).normalized();
}

template<typename T, uint_fast32_t V>
Matrix<T, V, 1> Triangle<T, V>::Normal(const Matrix<T, V, 1>& Point) const
{
    // Returns the linerally interpolated normal at the point
    Matrix<T, V, 1> bary = Barycentric(Point);
    return ((bary.x() * Normals.A) + (bary.y() * Normals.B) + (bary.z() * Normals.C)).normalized();
}

template<typename T, uint_fast32_t V>
bool Triangle<T, V>::On(const Matrix<T, V, 1>& Point) const
{
    // Calculates the barycentric coords of the point relative to the triangle (will sum to 1 for valid points on triangle)
    Matrix<T, V, 1> bary = Barycentric(Point);

    // Checks if the sum of the barycentric coords is one (avoiding floating point error), and if it is, then return true as that is a valid point
    if(fabs((bary.x() + bary.y() + bary.z()) - 1) <= FLOATING_POINT_ERROR<T>)
        return true;
    return false;
}

template<typename T, uint_fast32_t V>
Matrix<T, V, 1> Triangle<T, V>::Sides() const
{
    return Matrix<T, V, 1>((Vertices.A - Vertices.B).norm(), (Vertices.B - Vertices.C).norm(), (Vertices.C - Vertices.A).norm());
}

template<typename T, uint_fast32_t V>
T Triangle<T, V>::Area(const Triad<T, V>& Vertices)
{
    return (Vertices.B - Vertices.A).cross(Vertices.C - Vertices.A).norm() / 2;
}

// Explicitly defines templated instances
template class ArcaWave::Geometry::Triangle<float, 3>;
template class ArcaWave::Geometry::Triangle<double, 3>;
