#include <iostream>

#include "Geometry/Instance.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;

template<typename T, uint_fast32_t V>
Instance<T, V>::Instance(const uint_fast32_t InstanceIndex, const uint_fast32_t ObjectIndex, const InstanceType Type) :
	InstanceIndex(InstanceIndex), ObjectIndex(ObjectIndex), Type(Type), Pos()
{
	Vel = {Matrix<T, V, 1>(), Matrix<T, V, 1>()};
}

template<typename T, uint_fast32_t V>
Instance<T, V>::~Instance()
{}

template<typename T, uint_fast32_t V>
uint_fast32_t Instance<T, V>::GetInstanceIndex() const
{
	return InstanceIndex;
}

template<typename T, uint_fast32_t V>
uint_fast32_t Instance<T, V>::GetObjectIndex() const
{
	return ObjectIndex;
}

template<typename T, uint_fast32_t V>
InstanceType Instance<T, V>::GetType() const
{
	return Type;
}

template<typename T, uint_fast32_t V>
Pose<T, V> Instance<T, V>::GetPose() const
{
	return Pos;
}

template<typename T, uint_fast32_t V>
Velocity<T, V> Instance<T, V>::GetVelocity() const
{
	return Vel;
}

template<typename T, uint_fast32_t V>
bool Instance<T, V>::SetPose(const Pose<T, V>& Pos)
{
	// If the type is static, return failure, otherwise set the pose and return success
//	if(Type == InstanceType::STATIC)
//		return false;
	this->Pos = Pos;
	return true;
}

template<typename T, uint_fast32_t V>
bool Instance<T, V>::SetVelocity(const Velocity<T, V>& Vel)
{
	// If the type is static, return failure, otherwise set the velocity and return success
	if(Type == InstanceType::STATIC)
		return false;
	this->Vel = Vel;
	return true;
}

template<typename T, uint_fast32_t V>
Matrix<T, 4, 4> Instance<T, V>::GetTransformation() const
{
	Matrix<T, 4, 4> transform = Matrix<T, 4, 4>().setIdentity();
	transform.block(0, 0, 3, 3) = Pos.Orientation;
	transform(0, 3) = Pos.Position.x();
	transform(1, 3) = Pos.Position.y();
	transform(2, 3) = Pos.Position.z();
	return transform;
}

// @TODO handle angular velocity as well
template<typename T, uint_fast32_t V>
bool Instance<T, V>::ApplyVelocity(const T Duration)
{
	Pos.Position = Pos.Position + (Vel.Translational * Duration);
	return true;
}

template class ArcaWave::Geometry::Instance<float, 3>;
template class ArcaWave::Geometry::Instance<double, 3>;
