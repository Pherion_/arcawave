
#include "Geometry/Environment.h"

// STL Includes
#include <fstream>
#include <memory>
#include <iostream>

// JSON Includes
#include <nlohmann/json.hpp>

// ArcaWave Includes
#include "Geometry/Instance.h"
#include "Geometry/Object.h"
#include "Radar/RadarObject.h"

using namespace std;
using namespace ArcaWave::Geometry;

template<typename T, uint_fast32_t V>
Environment<T, V>::Environment()
{}

template<typename T, uint_fast32_t V>
Environment<T, V>::~Environment()
{}

template<typename T, uint_fast32_t V>
void Environment<T, V>::BuildEnvironment(const std::string& Path)
{
	nlohmann::json environment_json;
	ifstream(Path) >> environment_json;

	// @TODO JSON parsing exceptions
	// @TODO sotre objects in map as well?
	// Iterate over all model paths and load them into memory
	for (const string& object_path : environment_json[OBJECTS_KEY])
	{
	 	Objects.push_back(ArcaWave::Radar::RadarObject<T,V>::Import(object_path));

		shared_ptr<Radar::VCM<T, V>> vcm = reinterpret_pointer_cast<ArcaWave::Radar::RadarObject<T, V>>(Objects[Objects.size() - 1])->VCMSet[0];
		const uint_fast32_t size = vcm->Build->IncThetaInterval.Size * vcm->Build->IncPhiInterval.Size * vcm->Build->RefThetaInterval.Size * vcm->Build->RefPhiInterval.Size;
		for(uint_fast32_t i = 0; i < size; ++i)
		{
			vcm->Moments[i] = Matrix<std::complex<T>, V, 1>(std::complex<T>(1, 0), std::complex<T>(1, 0), std::complex<T>(1, 0));
		}
	}

	// TransmitterPos = Eigen::Matrix<T, V, 1>(environment_json[RADAR_KEY][POSITION_KEY].get<std::vector<T>>().data());
    // auto radar_ori = Eigen::Matrix<T, V, 1>(environment_json[RADAR_KEY][ORIENTATION_KEY].get<std::vector<T>>().data());
	// T angle = atan2(radar_ori.z(), radar_ori.x());
	// TransmitterOri = Eigen::Matrix<T, V, V>();
	// TransmitterOri << cos(angle), 0, sin(angle), 0, 1, 0, -sin(angle), 0, cos(angle);
	// cout << "Transmitter Orientation: " << TransmitterOri << endl << endl;

	// Iterate over all instance definitions and load them into memory
	for (const auto& instance : environment_json[INSTANCES_KEY])
	{
		uint_fast32_t object_index = instance["object_index"];
		bool instance_type = instance["is_dynamic"];

		shared_ptr<Instance<T, V>> inst;
		if(instance_type)
			inst = make_shared<Instance<T,V>>(Instances.size(), object_index, InstanceType::DYNAMIC);
		else
			inst = make_shared<Instance<T,V>>(Instances.size(), object_index, InstanceType::STATIC);

		Matrix<T, V, 1> pos((T)instance["position"][0], (T)instance["position"][1], (T)instance["position"][2]);
//		Matrix<T, V, V> ori;
//		for(uint_fast32_t i = 0; i < 9; ++i)
//			ori << (T)instance["orientation"][0];
		Matrix<T, V, V> ori = Map<Eigen::Matrix<T, V, V>> (instance["orientation"].get<std::vector<T>>().data());
		inst->SetPose(Pose<T, V>(pos, ori));
		Instances.push_back(inst);
	}
}

template<typename T, uint_fast32_t V>
void Environment<T, V>::BuildScene(const std::string& Path)
{
	   nlohmann::json scene_json;
	   ifstream(Path) >> scene_json;

	 // Iterate over array of instance descriptions
	 for (nlohmann::json& instance_description : scene_json[INSTANCE_DESC_KEY])
	 {
	 	vector<Pose<T, V>> poses;
	 	vector<Velocity<T,V>> velocities;

	 	uint_fast32_t instance_index = instance_description[INSTANCE_ID_KEY];

	 	// Fetch all poses for current instance
	 	for (nlohmann::json& pose : instance_description[POSES_KEY])
	 	{
	 		//Required to create a copy from Map.
	 		Eigen::Matrix<T, V, 1> pos_matrix = Eigen::Map<Eigen::Matrix<T, V, 1>> (pose[POSITION_KEY].get<std::vector<T>>().data());
	 		Eigen::Matrix<T, V, V> ori_matrix = Eigen::Map<Eigen::Matrix<T, V, V>> (pose[ORIENTATION_KEY].get<std::vector<T>>().data());

	 		poses.push_back(Pose<T, V>{pos_matrix, ori_matrix});
	 	}

	 	for (nlohmann::json& velocity : instance_description[VELOCITIES_KEY])
		{
	 		velocities.push_back(Velocity<T, V>
				{
					Eigen::Matrix<T, V, 1>(Eigen::Map<Eigen::Matrix<T, V, 1>> (velocity[TRANSLATIONAL_KEY].get<std::vector<T>>().data())),
					Eigen::Matrix<T, V, 1>(Eigen::Map<Eigen::Matrix<T, V, 1>> (velocity[ROTATIONAL_KEY].get<std::vector<T>>().data()))
				});
		}

	 	Velocities[instance_index] = velocities;
	 	Poses[instance_index] = poses;
	 }
}

template<typename T, uint_fast32_t V>
uint_fast32_t Environment<T, V>::AddInstance(shared_ptr<Instance<T, V>> Inst)
{
	uint_fast32_t idx = Instances.size();
	Instances.push_back(Inst);
	return idx;
}

template<typename T, uint_fast32_t V>
uint_fast32_t Environment<T, V>::AddObject(shared_ptr<Object<T, V>> Obj)
{
	uint_fast32_t idx = Objects.size();
	Objects.push_back(Obj);
	return idx;
}

template<typename T, uint_fast32_t V>
shared_ptr<Instance<T, V>> Environment<T, V>::GetInstance(uint_fast32_t Index) const
{
	return Instances[Index];
}

template<typename T, uint_fast32_t V>
uint_fast32_t Environment<T, V>::GetInstanceCount() const
{
	return Instances.size();
}

template<typename T, uint_fast32_t V>
shared_ptr<Object<T, V>> Environment<T, V>::GetObject(uint_fast32_t Index) const
{
	return Objects[Index];
}

template<typename T, uint_fast32_t V>
uint_fast32_t Environment<T, V>::GetObjectCount() const
{
	return Objects.size();
}

template<typename T, uint_fast32_t V>
void Environment<T, V>::UpdateEnvironment()
{
	//Update the state of all instances
	for (auto & instance : Instances)
	{
		if(instance->GetType() == InstanceType::DYNAMIC)
		{
			instance->SetPose(Poses[instance->GetInstanceIndex()][TimeStep]);
			instance->SetVelocity(Velocities[instance->GetInstanceIndex()][TimeStep]);
		}
	}

	//Increment the current timestep
	++TimeStep;
}

template<typename T, uint_fast32_t V>
void Environment<T, V>::ApplyVelocities(const T Duration)
{
	//Update the state of all dynamic instances
	for (auto & instance : Instances)
	{
		if (instance->GetType() == InstanceType::DYNAMIC)
		{
			instance->ApplyVelocity(Duration);
		}
	}
}

template<typename T, uint_fast32_t V>
void Environment<T, V>::ImportEnvironment(const string& Path)
{
	BuildEnvironment(Path);
	// BuildScene(Path);
	// UpdateEnvironment();
}

template<typename T, uint_fast32_t V>
const vector<std::shared_ptr<Object<T, V>>> &Environment<T, V>::GetObjects() const{ return Objects;}

template<typename T, uint_fast32_t V>
const vector<std::shared_ptr<Instance<T, V>>> &Environment<T, V>::GetInstances() const {return Instances;}

template class ArcaWave::Geometry::Environment<float, 3>;
template class ArcaWave::Geometry::Environment<double, 3>;
