#include "Geometry/Material.h"

// ArcaWave include
#include "Utility/Constants.h"

using namespace std;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Utility;

template<typename T>
Material<T>::Material() :
	permittivity(EPSILON_0<T>), permeability(MU_0<T>), conductivity(0)
{}

template<typename T>
Material<T>::Material(const T rel_permittivity, const T rel_permeability, const T conductivity) :
	permittivity(rel_permittivity * EPSILON_0<T>), permeability(rel_permeability * MU_0<T>), conductivity(conductivity)
{}

template<typename T>
Material<T>::~Material()
{}

template<typename T>
bool Material<T>::IsPEC() const
{	
	// If all 3 characters are 0, imported with default (0, 0, 0), then it is PEC
	return !permittivity && !permeability && !conductivity;
}

template<typename T>
complex<T> Material<T>::GetImpedance(T angular_freq) const
{
	// If material is vacuum, whose conductivity is 0, then impedance is Z_0<T>
	return sqrt(permeability / (permittivity + conductivity / (I<T> * angular_freq)));
}

template<typename T>
complex<T> Material<T>::GetWaveNumber(T angular_freq) const
{
	// If material is vacuum, whose conductivity is 0, then impedance is Z_0<T>
	return angular_freq * sqrt(permeability * (permittivity + conductivity / (I<T> * angular_freq)));
}

template class ArcaWave::Geometry::Material<float>;
template class ArcaWave::Geometry::Material<double>;