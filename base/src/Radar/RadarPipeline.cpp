
#include "Radar/RadarPipeline.h"

// STL includes
#include <chrono>
#include <iostream>

// ArcaWave includes
#include "Utility/Constants.h"
#include "Radar/RadarObject.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
RadarPipeline<T, V>::RadarPipeline(const RadarPipelineInfo<T, V>& Info) :
	Info(Info)
{}

template<typename T, uint_fast32_t V>
RadarPipeline<T, V>::RadarPipeline::~RadarPipeline()
{}

template<typename T, uint_fast32_t V>
void RadarPipeline<T, V>::RunFrameNearField(const shared_ptr<RadarFrame<T, V>>& Frame)
{
	// Computes the transmitted signal
	ComputeTXSignal(Frame);

	// Computes the received signal
	ComputeRXSignalNearField(Frame);

	// Mixes the transmitted and recieved signal
	// ComputeMXSignal(Frame);
}

template<typename T, uint_fast32_t V>
void RadarPipeline<T, V>::RunFrameFarField(const shared_ptr<RadarFrame<T, V>>& Frame)
{
	// Computes the transmitted signal
	ComputeTXSignal(Frame);

	// Computes the received signal
	ComputeRXSignalFarField(Frame);

	// Mixes the transmitted and recieved signal
	// ComputeMXSignal(Frame);
}

template<typename T, uint_fast32_t V>
void RadarPipeline<T, V>::ComputeTXSignal(const shared_ptr<RadarFrame<T, V>>& Frame)
{
	// Initializes the structure to hold the transmitted signal
	Frame->TXSignal = shared_ptr<Matrix<complex<T>, V, 1>[]>(new Matrix<complex<T>, V, 1>[Info.SampleCount * Info.TransmitterCount]);

	// For each sample time, go through each transmitter and record the outputed signal
	for (uint_fast32_t i = 0; i < Info.SampleCount; ++i)
	{
		const T time =  (i / Info.SampleRate);
		for (uint_fast32_t j = 0; j < Info.TransmitterCount; ++j)
			Frame->TXSignal[(i * Info.TransmitterCount) + j] = Info.RadField->ElectricField(time, Info.Transmitters[j]);
	}
}

template<typename T, uint_fast32_t V>
void RadarPipeline<T, V>::ComputeRXSignalNearField(const std::shared_ptr<RadarFrame<T, V>>& Frame)
{
	// Initializes the structure to hold the recieved signal
	// Frame->RXSignal = shared_ptr<Matrix<complex<T>, V, 1>[]>(new Matrix<complex<T>, V, 1>[Info.SampleCount * Info.ReceiverCount]);

	// TODO: Don't assume propogation or max frequency
	// Matrix<T, V, 1> prop(1, 0, 0);
	// T max_freq = 79e9;

	// Illuminates the environment
	// shared_ptr<IlluminateEnvironmentInfo<T, V>> illum_env_info = make_shared<IlluminateEnvironmentInfo<T, V>>();
	// illum_env_info->Type = IlluminationType::ILLUMINATION_DIRECTION;
	// illum_env_info->Vec = prop;
	// illum_env_info->Env = Info.Env;
	// shared_ptr<IlluminatedEnvironment<T, V>> illum_env = Info.RTX->IlluminatedEnvironment();

	// Discretizes the environment
	// shared_ptr<DiscretizedMesh<T, V>[]> disc_meshes = shared_ptr<Discretized
	// for(uint_fast32_t i = 0; )

	// Dispatch a job to calculate the recieved signal at each reciever
	// std::shared_ptr<NearFieldJobParameter> param = make_shared<NearFieldJobParameter>();
	// param->Info = Info;
	// param->Frame = Frame;
	// param->Position = pos;
	// param->Surface = surface;
	// Info.ThrdPool->Dispatch(Info.ThrdPool->GetMaxThreads(), JobFunction(NearFieldJobFunction), reinterpret_pointer_cast<JobParameter, NearFieldJobParameter>(param));
}

template<typename T, uint_fast32_t V>
void RadarPipeline<T, V>::ComputeRXSignalFarField(const std::shared_ptr<RadarFrame<T, V>>& Frame)
{
	// Initializes the structure to hold the recieved signal
	cout << "THIS IS WHERE WE ARE" << endl;
	Frame->RXSignal = shared_ptr<Matrix<complex<T>, V, 1>[]>(new Matrix<complex<T>, V, 1>[Info.SampleCount * Info.ReceiverCount]);
	memset(Frame->RXSignal.get(), 0, sizeof(Matrix<complex<T>, V, 1>) * Info.SampleCount * Info.ReceiverCount);

	auto launch = LaunchInfo<T, V>();
	launch.FOV = 45;
	launch.Height  = 1000;
	launch.Width = 1000;
	launch.WinWidth = 2;
	launch.WinHeight = 2;
	launch.FocalLen = 0.5;
	launch.Pos = Info.Transmitters[0];

	// Goes through each ray created in the window
	auto hits = Info.RTX->ProjectRays(launch);
	for (const ProjectedRay<T, V>& projected : hits)
	{
	    /*
		if (!projected.Contact || !projected.RxVisible)  continue;
		//const shared_ptr<Instance<T, V>> inst = Info.Env->GetInstance(projected.InstanceIndex);
		const shared_ptr<RadarObject<T, V>> obj =reinterpret_pointer_cast<RadarObject<T, V>, Object<T, V>>(Info.Env->GetObject(inst->GetObjectIndex()));

		// Gets the incidence vector on the VCM
		//const Matrix<T, V, 1> inc_dir = (inst->GetPose().Orientation.inverse() * projected.PropVector).normalized();
		const T inc_theta = atan2(.z(), inc_dir.x());
		const T inc_phi = atan2(sqrt((inc_dir.x() * inc_dir.x()) + (inc_dir.z() * inc_dir.z())), inc_dir.y());
		const Matrix<T, 2, 1> inc(inc_theta, inc_phi);


		// Calculates the far field vector for each receiver
		for (uint_fast32_t i = 0; i < Info.ReceiverCount; ++i)
		{
			// Gets the location of the receiver relative to the VCM
			const Matrix<T, V, 1> rev_dir = inst->GetPose().Orientation.inverse() * (Info.Receivers[i] - projected.Point);

			// Calculates the theta and phi angles to the reciever relative to the  (adjust for arctan range)
			const T ref_theta = atan2(rev_dir.z(), rev_dir.x());
			const T ref_phi = atan2(sqrt((rev_dir.x() * rev_dir.x()) + (rev_dir.z() * rev_dir.z())), rev_dir.y());
			const T ref_dist = rev_dir.norm();
			const Matrix<T, 2, 1> ref(ref_theta, ref_phi);

			// Gets the theta and phi unit vectors
			const Matrix<complex<T>, V, 1> theta_unit(-sin(ref_theta), cos(ref_theta), 0);
			// const Matrix<complex<T>, V, 1> theta_unit(-sin(ref_theta), 0, cos(ref_theta));
			const Matrix<complex<T>, V, 1>
				phi_unit(cos(ref_theta) * cos(ref_phi), sin(ref_theta) * cos(ref_phi), -sin(ref_phi));
				// phi_unit(cos(ref_theta) * cos(ref_phi), -sin(ref_phi), sin(ref_theta) * cos(ref_phi));

			// Goes through each sample of that reciever
			for (uint_fast32_t f = 0; f < Info.SampleCount; ++f)
			{
				// Gets the time to collect the sample
				const T sample_time = (f / Info.SampleRate);

				// Gets the moment at the VCM to the reciever
				// @TODO MESH LOOKUP
				//const Matrix<complex<T>, V, 1> moment = obj->GetFaceVCM(0, 0)->GetMoment(inc, ref);
				Matrix<complex<T>, V, 1> moment = {1, 1, 1};

				// TODO: Go through each transmitter and find its contribution to the recieved signal
				T time = sample_time - (ref_dist / C_0<T>);
				if (time < 0) continue;
				T phase = Info.RadField->Phase(time, projected.Dist);
				// T phase = Info.RadField->DopplerPhase(time, projected.Dist, RelVel);
				complex<T> wave = std::exp(I<T> * phase);
				Matrix<complex<T>, V, 1> rx = moment * wave;
				// T gain = Info.RadField->Gain(inc_dir);
				// Matrix<complex<T>, V, 1> rx = gain * moment * wave;
				// Adds the rays contribution to the sample normalizing by ray count
				Frame->RXSignal[(f * Info.ReceiverCount) + i] += rx / (launch.Width * launch.Height);
			}
		}*/
	}

}


template<typename T, uint_fast32_t V>
void RadarPipeline<T, V>::ComputeMXSignal(const shared_ptr<RadarFrame<T, V>>& Frame)
{
	// Initializes the structure to hold the mixed signal
	Frame->MXSignal = shared_ptr<Matrix<complex<T>, V, 1>[]>(new Matrix<complex<T>, V, 1>[Info.SampleCount * Info.ReceiverCount * Info.TransmitterCount]);

	// For each sample, mix the TX and RX signals
	for (uint_fast32_t i = 0; i < Info.SampleCount; ++i)
	{
		// TODO: Adjust for multiple transmitters
		const Matrix<complex<T>, V, 1> tx = Frame->TXSignal[i];

		// Go through each receiver and mix its signal with the transmitted signal
		for (uint_fast32_t j = 0; j < Info.ReceiverCount; ++j)
		{
			// Get the recieved signal for the sample
			const Matrix<complex<T>, V, 1> rx = Frame->RXSignal[(i * Info.ReceiverCount) + j];

			// Mix the transmitted and recieved signals
			Frame->MXSignal[(i * Info.ReceiverCount) + j] = (rx.real().array() * tx.real().array()) - (I<T> * rx.real().array() * tx.imag().array());
		}
	}
}

// template<typename T, uint_fast32_t V>
// void RadarPipeline<T, V>::SampleNearField(RadarFrame<T, V>& Frame)
// {
//     uint_fast32_t rx_count = Frame.Receivers->size();
//     Frame.RX = make_shared<vector<Matrix<complex<T>, V, 1>>>(rx_count);

//     // Define characteristics of two media, should not be hardcoded
//     // Assuming vacuum for first media, second media defined in .mtl
//     T permittivity_1 = EPSILON_0<T>;
//     T permeability_1 = MU_0<T>;
//     T conductivity_1 = 0;
//     T impedance_1 = Z_0<T>;
//     T permittivity_2 = Frame.Surface->Materials[0];
//     T permeability_2 = Frame.Surface->Materials[1];
//     T conductivity_2 = Frame.Surface->Materials[2];

//     // Sample at each given position 
//     for(uint_fast32_t i = 0; i < rx_count; ++i)
//     {
//         // Calculates scatter field at particular point by integrating over the surface of the mesh
//         Matrix<T, V, 1> r = Frame.Receivers->at(i);
//         Matrix<complex<T>, V, 1> sca(0, 0, 0);
//         Matrix<T, V, 1> r_unit = r / r.norm();

//         for(uint_fast32_t j = 0; j < Frame.Surface->Vertices.size(); ++j)
//         {
//             // Determine the distance to each point on the surface and the time at which the recieving signal bounced off that point
//             const Matrix<T, V, 1>& v = Frame.Surface->Vertices[j];
//             T dist = (r - v).norm();
//             T time = Frame.Time - (dist / C_0<T>);
//             T freq = Frame.Rad->Frequency(time, v);
//             T angular_freq = 2 * PI<T> * freq;
//             T lambda = C_0<T> / freq;
//             complex<T> impedance_2 = sqrt(permeability_2 / (permittivity_2 + conductivity_2 / (I<T> * angular_freq)));

//             // Calculate the surface current at that point approximated by MECA
//             Matrix<complex<T>, V, 1> E_inc = Frame.Rad->ElectricField(time, v);

//             // Unit vector calculation
//             Matrix<T, V, 1> n_unit = Frame.Surface->Normals[j];
//             Matrix<T, V, 1> k_unit_inc(1, 0, 0); // unit incident vector, hardcoded now, should be replaced
//             Matrix<T, V, 1> e_unit_TE = k_unit_inc.cross(n_unit) / k_unit_inc.cross(n_unit).norm();
//             Matrix<T, V, 1> e_unit_TM = e_unit_TE.cross(k_unit_inc);

//             // Cosine of angle of incidence
//             T cos_theta_inc = -k_unit_inc.dot(n_unit) / (k_unit_inc.norm() * n_unit.norm());

//             // Coordinate transformation
//             complex<T> E_inc_TE = E_inc.dot(e_unit_TE);
//             complex<T> E_inc_TM = E_inc.dot(e_unit_TM);

//             // Reflection coefficients
//             // (1) PEC
//             // T R_TE = -1;
//             // T R_TM = -1;

//             // (2) Non-metallic and metallic
//             // The refractive index, wavenumber, impedance and angle are all complex number, 
//             // the imaginary part describes attenuation
//             complex<T> refractive_index_inc_over_tra = permeability_1 * permittivity_1
//                 / (permeability_2 * (permittivity_2 + conductivity_2 / (I<T> * angular_freq)));
//             T cos_theta_inc_over_tra = cos_theta_inc / sqrt(1
//                 - refractive_index_inc_over_tra.real() * (1 - cos_theta_inc * cos_theta_inc));
//             complex<T> R_TE = complex<T>(2) / (complex<T>(1) + impedance_1 / (impedance_2 * cos_theta_inc_over_tra)) - complex<T>(1);
//             complex<T> R_TM = complex<T>(2) / (complex<T>(1) + impedance_1 / impedance_2 * cos_theta_inc_over_tra) - complex<T>(1);

//             // Calculate electric and magnetic current densities
//             Matrix<complex<T>, V, 1> J_MECA =
//             	(E_inc_TE * cos_theta_inc * (complex<T>(1) - R_TE) * e_unit_TE
//             	+ E_inc_TM * (complex<T>(1) - R_TM) * (n_unit.cross(e_unit_TE)))
//             	/ Z_0<T>;

//             Matrix<complex<T>, V, 1> M_MECA =
//             	(E_inc_TE * (complex<T>(1) + R_TE) * (e_unit_TE.cross(n_unit))
//             	+ E_inc_TM * cos_theta_inc * (complex<T>(1) + R_TM) * e_unit_TE);

//             // Compute the scatter field contribution of the point and add it to the integration sum

//             // Wrong way!
//             // Magnitude of the vector between the source and the observation is approximated as observation position vector
//             // Didn't remove the e^{ikr} term, reserved as the original one in Mert's paper
//             // T G_phase = 2 * PI<T> / lambda * (r_unit.dot(v) - r.norm());
//             // complex<T> G_e = cos(G_phase) + I<T> * sin(G_phase);
//             // sca += G_e / r.norm() / lambda * Frame.Surface->Areas[j]
//             //     * (r_unit.cross(M_MECA) - Z_0<T> * r_unit.cross(J_MECA).cross(r_unit));

//             // Remove the e^{ikr} term, and not use dist approximation
//             sca += (r_unit.cross(M_MECA) - Z_0<T> * r_unit.cross(J_MECA).cross(r_unit))
//                 / dist / lambda * Frame.Surface->Areas[j];
//         }

//         // Scale the final result of the integration and store it
//         (*Frame.RX)[i] = sca * I<T> / 2;
//     }
// }

template<typename T, uint_fast32_t V>
void RadarPipeline<T, V>::NearFieldJobFunction(uint_fast32_t ThreadID, uint_fast32_t ThreadCount, const shared_ptr<JobParameter>& Parameter)
{
	// // Casts the parameter into the correct type
	// const shared_ptr<NearFieldJobParameter> param = reinterpret_pointer_cast<NearFieldJobParameter, JobParameter>(Parameter);

	// // Defines the workload for the thread so that the entire job is handled (the last thread may have less work to do)
	// const uint_fast32_t workload = (param->Info.SampleCount % ThreadCount == 0) ? (param->Info.SampleCount / ThreadCount) : (param->Info.SampleCount / ThreadCount + 1);
	// const uint_fast32_t start = ThreadID * workload;
	// const uint_fast32_t end = min(param->Info.SampleCount, (ThreadID + 1) * workload);

	// // Handles the workload for the thread
	// for(uint_fast32_t sample = start; sample < end; ++sample)
	// {
	//     // Sample at each receiver
	//     const T sample_time = param->Frame->Time + (sample / param->Info.SampleRate);
	//     for(uint_fast32_t i = 0; i < param->Info.ReceiverCount; ++i)
	//     {
	//         const Matrix<T, V, 1> r = param->Info.Receivers[i];
	//         Matrix<complex<T>, V, 1> sca(0, 0, 0);

	//         // TODO: Fix for multiple instances

	//         for (uint_fast32_t j = 0; j < param->Surface->Vertices.size(); j++)
	//         {
	//             // TODO: Take into account position and orientation better

	//             // Determine the distance to each point on the surface and the time at which the recieving signal bounced off that point
	//             const Matrix<T, V, 1> position(param->Surface->Vertices[j] + param->Position);
	//             const T dist = (r - position).norm();
	//             const T time = sample_time - (dist / C_0<T>);

	//             // Calculate the surface current at that point approximated by physical optics
	//             const Matrix<complex<T>, V, 1> current = 2 * static_cast<Matrix<complex<T>, V, 1>>(param->Surface->Normals[j]).cross(param->Info.RadField->MagneticField(time, position));

	//             // Compute the scatter field contribution of the point and add it to the integration sum
	//             const  T freq = param->Info.RadField->Frequency(time, position);
	//             sca += current * param->Surface->Areas[j] * freq / dist;
	//         }

	//         // Scale the final result of the integration and store it in the output signal
	//         param->Frame->RXSignal[(sample * param->Info.ReceiverCount) + i] = sca * MU_0<T> * I<T> / 2;
	//     }
	// }
}

// COMPUTE RCS
// template<typename T, uint_fast32_t V>
// void RadarPipeline<T, V>::ComputeRCS(RadarFrame<T, V>& Frame)
// {
//     uint_fast32_t res = 360;
//     Frame.RCS = make_shared<vector<T>>(res);
//     Matrix<complex<T>, V, 1> polarization(0, 0, 1);

//     for(uint_fast32_t i = 0; i < res; ++i)
//         (*Frame.RCS)[i] = norm(polarization.dot((*Frame.VectorPotential)[i])) * 4 * PI<T>;
// }

// Explicitly defines templated instances
template class ArcaWave::Radar::RadarPipeline<float, 3>;
template class ArcaWave::Radar::RadarPipeline<double, 3>;
