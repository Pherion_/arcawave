#include "Radar/Noise/NoiseSource.h"

// STL includes
#include <cmath>
#include <random>

// ArcaWave incldues

using namespace std;
using namespace Eigen;
using namespace ArcaWave;

using namespace ArcaWave::Radar;

template<typename T, uint_fast32_t V>
NoiseSource<T,V>::NoiseSource(uint_fast32_t seed,T averagePower) : Generator{seed}, AveragePower{averagePower}
{

}
template<typename T, uint_fast32_t V>
NoiseSource<T, V>::~NoiseSource()
= default;

template<typename T, uint_fast32_t V>
T NoiseSource<T, V>::GetAveragePower() const
{
	return AveragePower;
}

template<typename T, uint_fast32_t V>
void NoiseSource<T, V>::SetAveragePower(T averagePower)
{
	AveragePower = averagePower;
}

// Explicitly defines templated instances
template class ArcaWave::Radar::NoiseSource<float, 3>;
template class ArcaWave::Radar::NoiseSource<double, 3>;
