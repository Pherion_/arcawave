#include "Radar/Noise/BandLimitiedNoiseSource.h"
#include "Radar/Noise/NoiseSource.h"

// STL includes
#include <random>
#include <cmath>
#include <mutex>
#include <shared_mutex>

// ArcaWave incldues
#include "Utility/Constants.h"

using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
Eigen::Matrix<T, V, 1> BandLimitedNoiseSource<T, V>::Noise(uint_fast32_t index)
{
	if (index % Granularity == 0)
	{
		// Acquire write lock if changing the set of frequencies and amplituded
		std::unique_lock lock(GranularityLock);
		CurrentFrequency.clear();
		for (uint_fast32_t i = 0; i <Granularity ; i++) CurrentFrequency.push_back(UniformDistribution(NoiseSource<T,V>::Generator));
		CurrentAmplitude = GaussianDistribution(NoiseSource<T,V>::Generator);
	}

	// Acquire read lock at all other times
	std::shared_lock lock(GranularityLock);

	std::vector<T> noise_vector;
	for (uint_fast32_t i = 0; i < V; i++)
	{
		T noise_data = 0;
		for (uint_fast32_t j = 0; j < Granularity; j++) noise_data += (CurrentAmplitude/Granularity) * std::sin(2 * PI<T> * CurrentFrequency[j] * (index / SamplingRate));
		noise_vector.push_back(noise_data);
	}

	return Eigen::Matrix<T, V, 1>(noise_vector.data());
}

template<typename T, uint_fast32_t V>
BandLimitedNoiseSource<T, V>::~BandLimitedNoiseSource()
= default;

template<typename T, uint_fast32_t V>
void BandLimitedNoiseSource<T, V>::SetAveragePower(T averagePower)
{
	// Acquire write lock before changing internal state;
	std::unique_lock lock(GranularityLock);
	NoiseSource<T,V>::SetAveragePower(averagePower * 2 * PI<T>);
	GaussianDistribution = std::normal_distribution<T>(0, averagePower);
	CurrentAmplitude = GaussianDistribution(NoiseSource<T,V>::Generator);
}

template<typename T, uint_fast32_t V>
void BandLimitedNoiseSource<T, V>::SetBandwidth(T minFreq, T maxFreq)
{
	// Acquire write lock before changing internal state;
	std::unique_lock lock(GranularityLock);
	UniformDistribution = std::uniform_real_distribution<T>(minFreq, maxFreq);
	CurrentFrequency.clear();
	for (uint_fast32_t i =0; i < Granularity; i++) CurrentFrequency.push_back(UniformDistribution(NoiseSource<T,V>::Generator));
}

template<typename T, uint_fast32_t V>
BandLimitedNoiseSource<T, V>::BandLimitedNoiseSource(uint_fast32_t seed, T averagePower, T minFreq, T maxFreq, uint_fast32_t granularity) :
	NoiseSource<T,V>(seed, averagePower),
	Granularity(granularity),
	SamplingRate(2*maxFreq),
	UniformDistribution(std::uniform_real_distribution<T>(minFreq, maxFreq)),
	GaussianDistribution(std::normal_distribution<T>(0, (averagePower * 2 * PI<T>)))
{
	for (uint_fast32_t i = 0; i < Granularity; i ++) CurrentFrequency.push_back(UniformDistribution(NoiseSource<T,V>::Generator));
	CurrentAmplitude = GaussianDistribution(NoiseSource<T,V>::Generator);
}

// Explicitly defines templated instances
template class ArcaWave::Radar::BandLimitedNoiseSource<float, 3>;
template class ArcaWave::Radar::BandLimitedNoiseSource<double, 3>;

