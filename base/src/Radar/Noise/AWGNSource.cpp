#include "Radar/Noise/AWGNSource.h"

// STL includes
#include <random>
#include <iostream>

using namespace ArcaWave::Radar;

template<typename T, uint_fast32_t V>
Eigen::Matrix<T, V, 1> AWGNSource<T, V>::Noise(uint_fast32_t index)
{
	std::vector<T> noise_data;

	for (uint_fast32_t i = 0; i < V; i ++)
	{
		noise_data.push_back(GaussianDistribution(NoiseSource<T,V>::Generator));
	}

	return Eigen::Matrix<T, V, 1>(noise_data.data());
}

template<typename T, uint_fast32_t V>
AWGNSource<T, V>::~AWGNSource()
= default;

template<typename T, uint_fast32_t V>
void AWGNSource<T, V>::SetAveragePower(T averagePower)
{
	NoiseSource<T,V>::SetAveragePower(averagePower);
	GaussianDistribution = std::normal_distribution<T>(0, averagePower);
}

template<typename T, uint_fast32_t V>
AWGNSource<T, V>::AWGNSource(uint_fast32_t seed, T averagePower):NoiseSource<T,V>(seed, averagePower)
{
	AWGNSource<T,V>::SetAveragePower(averagePower);
}


// Explicitly defines templated instances
template class ArcaWave::Radar::AWGNSource<float, 3>;
template class ArcaWave::Radar::AWGNSource<double, 3>;
