#include "Radar/DSP/FrequencyDSP.h"

// STL includes
#include <cmath>
#include <iostream>

// ArcaWave incldues
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
FrequencyDSP<T, V>::FrequencyDSP(const SignalProcessingInfo<T, V>& Info, const shared_ptr<Matrix<complex<T>, V, 1>[]> Data) :
    SignalProcessing<T ,V>(Info, Data)
{}

template<typename T, uint_fast32_t V>
FrequencyDSP<T, V>::~FrequencyDSP()
{}

template<typename T, uint_fast32_t V>
void FrequencyDSP<T, V>::RangeEstimation()
{
    
}

template<typename T, uint_fast32_t V>
void FrequencyDSP<T, V>::AngleEstimation()
{
    
}

template<typename T, uint_fast32_t V>
void FrequencyDSP<T, V>::VelocityEstimation()
{
    
}

// Explicitly defines templated instances
template class ArcaWave::Radar::FrequencyDSP<float, 3>;
template class ArcaWave::Radar::FrequencyDSP<double, 3>;
