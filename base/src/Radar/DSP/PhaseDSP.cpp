#include "Radar/DSP/PhaseDSP.h"

// STL includes
#include <cmath>
#include <iostream>

// ArcaWave incldues
#include "Utility/Constants.h"
#include "Radar/Radiation/PMCW.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

// namespace ArcaWave::Radar::CUDA
// {
//     template<typename T, uint_fast32_t V>
//     __global__ void CalculateSamples(ArcaWave::Geometry::ProjectedRay<T, V>* HitInfo, ArcaWave::Radar::DeviceRadarPipelineInfo<T, V> RadInfo, Eigen::Matrix<thrust::complex<T>, V, 1>* Output)
//     {
//         uint32_t i = blockIdx.x;
//         uint32_t f = threadIdx.x;
//         const T sample_time = (f / RadInfo.SampleRate);
//         T time = sample_time - ((RadInfo.Receivers[i] - HitInfo->Point).norm() / ArcaWave::Utility::C_0<T>);
//         if (time > 0)
//         {
//             T phase = RadInfo.RadField->Phase(time, HitInfo->Dist);
//             thrust::complex<T> wave = thrust::exp(thrust::complex<T>(0, 1) * phase);
//             auto rx = HitInfo->Amplitude * wave;
//             ArcaWave::Utility::CUDA::AtomicComplexVector<T, V>(&(Output[f * RadInfo.ReceiverCount + i]), rx);
//         }
//     }

//     template <typename T, uint_fast32_t V>
//     __global__ void ProcessHits(ArcaWave::Geometry::ProjectedRay<T, V>* Buffer, ArcaWave::Radar::DeviceRadarPipelineInfo<T, V> RadInfo, Eigen::Matrix<thrust::complex<T>, V, 1>* Output)
//     {
//         int block_id = blockIdx.x + blockIdx.y * gridDim.x;
//         int thread_id = block_id * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
//         if (Buffer[thread_id].RxVisible && Buffer[thread_id].Contact)
//         {
//             printf("visible and contact: %d %d\n", Buffer[thread_id].RxVisible, Buffer[thread_id].Contact);
//             CalculateSamples<T, V><<<RadInfo.ReceiverCount, RadInfo.SampleCount>>>(&(Buffer[thread_id]), RadInfo, Output);
//         }
//     }
// }

template<typename T, uint_fast32_t V>
PhaseDSP<T, V>::PhaseDSP(const SignalProcessingInfo<T, V>& Info, const shared_ptr<Matrix<complex<T>, V, 1>[]> Data) :
    SignalProcessing<T ,V>(Info, Data)
{}

template<typename T, uint_fast32_t V>
PhaseDSP<T, V>::~PhaseDSP()
{}

template<typename T, uint_fast32_t V>
void PhaseDSP<T, V>::RangeEstimation()
{
    shared_ptr<Matrix<T, Dynamic, 1>[]> ElectricField = shared_ptr<Matrix<T, Dynamic, 1>[]>(new Matrix<T, Dynamic, 1>[Info.frame_count * Info.chirp_count]);
    for (uint_fast32_t i = 0; i < Info.frame_count; ++i)
    {
        for (uint_fast32_t j = 0; j < Info.chirp_count; ++j)
        {
            for (uint_fast32_t k = 0; k < Info.SampleCount; ++k)
            {
                uint_fast32_t index = i * Info.chirp_count + j;
                uint_fast32_t index_data = index * Info.SampleCount + k;
                ElectricField[index].resize(Info.SampleCount);
                ElectricField[index][k] = Matrix<T, V, 1>(Data[index_data][0].real(), Data[index_data][1].real(), Data[index_data][2].real()).transpose() * ElectricFieldDirection;
            }
        }
    }
    uint_fast32_t start = 2;
    uint_fast32_t a = start;
    uint_fast32_t period = 1;
    string prbs;
    uint_fast32_t mask = (1 << 10) - 1;
    while (true) {
        uint_fast32_t bit = (((a >> 9) ^ (a >> 6)) & 1);
        a = ((a << 1) | bit) & mask;
        prbs += bit + '0';
        if (a == start)
            break;
        period++;
    }
    shared_ptr<Matrix<T, Dynamic, 1>[]> Bank = shared_ptr<Matrix<T, Dynamic, 1>[]>(new Matrix<T, Dynamic, 1>[period]);
    for (uint_fast32_t i = 0; i < period; ++i)
    {
        Bank[i].resize(period);
        // Shift
        for (uint_fast32_t j = 0; j < i; ++j)
            Bank[i][j] = (prbs[period-i+j] - '0') * PI<T>;
        for (uint_fast32_t j = i; j < period; ++j)
            Bank[i][j] = (prbs[j-i] - '0') * PI<T>;

    }
}

template<typename T, uint_fast32_t V>
void PhaseDSP<T, V>::AngleEstimation()
{
    
}

template<typename T, uint_fast32_t V>
void PhaseDSP<T, V>::VelocityEstimation()
{
    
}

// Explicitly defines templated instances
template class ArcaWave::Radar::PhaseDSP<float, 3>;
template class ArcaWave::Radar::PhaseDSP<double, 3>;
