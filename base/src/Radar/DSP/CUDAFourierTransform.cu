#include "Radar/DSP/CUDAFourierTransform.h"
#include <stdint.h>
#include <type_traits>

using namespace std;
using namespace ArcaWave;
using namespace Eigen;
using namespace ArcaWave::Radar;

#define TILE_DIM 8
#define fft_size 181


namespace ArcaWave
{
	namespace Radar
	{
		namespace CUDA
		{

			
			// Naive matrix transpose xyz -> zyx
			template <typename T, uint_fast32_t V>
			__global__ void transposeZYX(complex<T> *odata, complex<T> *idata, int dim_X, int dim_Y, int dim_Z)
			{
				
				const int blockIdx_x = blockIdx.x;
				const int blockIdx_y = blockIdx.y;
				const int blockIdx_z = blockIdx.z;

				const int x = blockIdx_x*TILE_DIM + threadIdx.x;
				const int y = blockIdx_y*TILE_DIM + threadIdx.y;
				const int z = blockIdx_z*TILE_DIM + threadIdx.z;

				if (x < dim_X && y < dim_Y && z < dim_Z) {

					const int idata_index = x + y*dim_X + z*dim_X*dim_Y;
					const int odata_index = z + y*dim_Z + x*dim_Z*dim_Y;

					odata[odata_index] = idata[idata_index];
				}
			}

			// Naive matrix transpose xyz -> yxz
			template <typename T, uint_fast32_t V>
			__global__ void transposeYXZ(complex<T> *odata, complex<T> *idata, int dim_X, int dim_Y, int dim_Z)
			{
				
				const int blockIdx_x = blockIdx.x;
				const int blockIdx_y = blockIdx.y;
				const int blockIdx_z = blockIdx.z;

				const int x = blockIdx_x*TILE_DIM + threadIdx.x;
				const int y = blockIdx_y*TILE_DIM + threadIdx.y;
				const int z = blockIdx_z*TILE_DIM + threadIdx.z;

				if (x < dim_X && y < dim_Y && z < dim_Z) {

					const int idata_index = x + y*dim_X + z*dim_X*dim_Y;
					const int odata_index = y + x*dim_Y + z*dim_X*dim_Y;

					odata[odata_index] = idata[idata_index];
				}
			}

			// Naive zero padding on first axis to pad_dim
			template <typename T, uint_fast32_t V>
			__global__ void zeroPadding(complex<T> *odata, complex<T> *idata, int dim_X, int dim_Y, int dim_Z, int pad_dim)
			{
				
				const int blockIdx_x = blockIdx.x;
				const int blockIdx_y = blockIdx.y;
				const int blockIdx_z = blockIdx.z;

				const int x = blockIdx_x*TILE_DIM + threadIdx.x;
				const int y = blockIdx_y*TILE_DIM + threadIdx.y;
				const int z = blockIdx_z*TILE_DIM + threadIdx.z;

				if (x < dim_X && y < dim_Y && z < dim_Z) {

					const int idata_index = x + y*dim_X + z*dim_X*dim_Y;
					const int odata_index = x + y*pad_dim + z*dim_Y*pad_dim;

					odata[odata_index] = idata[idata_index];
				}else{
					if(x < pad_dim && y < dim_Y && z < dim_Z){
						const int odata_index =  x + y*pad_dim + z*dim_Y*pad_dim;
						odata[odata_index] = complex<T> (0,0);

					}
				}
			}

			// Naive fftshift  
			template <typename T, uint_fast32_t V>
			__global__ void fftShift(complex<T> *odata, complex<T> *idata, int dim_X, int dim_Y, int dim_Z)
			{
				
				const int blockIdx_x = blockIdx.x;
				const int blockIdx_y = blockIdx.y;
				const int blockIdx_z = blockIdx.z;

				const int x = blockIdx_x*TILE_DIM + threadIdx.x;
				const int y = blockIdx_y*TILE_DIM + threadIdx.y;
				const int z = blockIdx_z*TILE_DIM + threadIdx.z;

				if (x < dim_X && y < dim_Y && z < dim_Z) {

					const int odata_index = ((x+dim_X/2)%dim_X) + ((y+dim_Y/2)%dim_Y)*dim_X + ((z+dim_Z/2)%dim_Z)*dim_X*dim_Y;
					const int idata_index = x + y*dim_X + z*dim_Y*dim_X;

					odata[odata_index] = idata[idata_index];
				}
			}

		}
	}
}

template <typename T, uint_fast32_t V>
CUDAFourierTransform<T, V>::CUDAFourierTransform(std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> Mixed, Radar::DSPprocess<T,V>& info)
{
	thrust::device_vector<complex<T>> d_rawdata(info.r_rows*info.r_cols*info.num_samples);
	num_samples = info.num_samples;
	uint_fast32_t jump = info.r_rows*info.r_cols;

	/* moving simulation data into a dsp data structure easy for the first round of dsp use */
	for(int i = 0; i < info.r_rows; i++)
	{
		for(int j = 0; j < info.r_cols; j++)
		{
			uint_fast32_t idx = i * info.r_rows + j;
			for(int s = 0; s < info.num_samples; s++)
			{
				d_rawdata[i*info.r_cols*info.num_samples+j*info.num_samples+s] = Mixed[s *jump + idx].z();
			}
		}
	}
	Processing(info,d_rawdata);
}


template<typename T, uint_fast32_t V>
void CUDAFourierTransform<T, V>::Processing( Radar::DSPprocess<T,V>& info, thrust::device_vector<complex<T>>& d_rawdata)
{

    const uint_fast32_t input_size = info.r_cols * info.r_rows *  info.num_samples;
    const uint_fast32_t output_size = fft_size * fft_size *  info.num_samples;
	auto current_dev = cuda::device::get(0);
	current_dev.synchronize();

	thrust::device_vector<complex<T>> d_range_out(input_size);
	thrust::device_vector<complex<T>> d_range_padding(fft_size * info.r_cols * info.num_samples);
	thrust::device_vector<complex<T>> d_azimuth_out(fft_size * info.r_cols * info.num_samples);
	thrust::device_vector<complex<T>> d_azimuth_padding(output_size);
	thrust::device_vector<complex<T>> d_elevation_out(output_size);
	thrust::device_vector<complex<T>> d_data_out(output_size);



	// Range FFT : 
	// info.num_samples info.r_cols info.r_rows -> info.num_samples info.r_cols info.r_rows
	CufftProcessing((int)info.num_samples ,(int)info.r_cols * (int)info.r_rows,d_rawdata,d_range_out);
	current_dev.synchronize();

	// Transpose XYZ -> ZYX: info.num_samples info.r_cols info.r_rows -> info.r_rows info.r_cols info.num_samples
	auto launch_size = cuda::make_launch_config(cuda::grid::dimensions_t((int)info.num_samples/TILE_DIM+1,(int) info.r_cols/TILE_DIM+1, (int) info.r_rows/TILE_DIM+1), cuda::grid::dimensions_t(TILE_DIM,TILE_DIM,TILE_DIM));
	current_dev.launch(ArcaWave::Radar::CUDA::transposeZYX<T, V>,
	    launch_size,
		(thrust::raw_pointer_cast(d_rawdata.data())),(thrust::raw_pointer_cast(d_range_out.data())),(int)info.num_samples,(int) info.r_cols,(int) info.r_rows
	    );
	current_dev.synchronize(); 

	// Zero Padding on first axis: info.r_rows info.r_cols info.num_samples -> fft_size info.r_cols info.num_samples
	launch_size = cuda::make_launch_config(cuda::grid::dimensions_t(fft_size/TILE_DIM+1, (int) info.r_cols/TILE_DIM+1,(int)info.num_samples/TILE_DIM+1), cuda::grid::dimensions_t(TILE_DIM,TILE_DIM,TILE_DIM));
	current_dev.launch(ArcaWave::Radar::CUDA::zeroPadding<T, V>,
	    launch_size,
		(thrust::raw_pointer_cast(d_range_padding.data())),(thrust::raw_pointer_cast(d_rawdata.data())),(int) info.r_rows,(int) info.r_cols,(int)info.num_samples,fft_size
	    );
	current_dev.synchronize();




	// Azimuth FFT : 
	// fft_size info.r_cols info.num_samples -> fft_size info.r_cols info.num_samples 
	CufftProcessing(fft_size ,(int)info.num_samples*(int)info.r_cols,d_range_padding,d_azimuth_out);
	current_dev.synchronize();

	// Transpose ZYX -> XYZ:  fft_size info.r_cols info.num_samples -> info.num_samples info.r_cols fft_size 
	launch_size = cuda::make_launch_config(cuda::grid::dimensions_t(fft_size/TILE_DIM+1,(int) info.r_cols/TILE_DIM+1, (int)info.num_samples/TILE_DIM+1), cuda::grid::dimensions_t(TILE_DIM,TILE_DIM,TILE_DIM));
	current_dev.launch(ArcaWave::Radar::CUDA::transposeZYX<T, V>,
	    launch_size,
		(thrust::raw_pointer_cast(d_range_padding.data())),(thrust::raw_pointer_cast(d_azimuth_out.data())),fft_size,(int) info.r_cols,(int)info.num_samples
	    );
	current_dev.synchronize();

	// Transpose XYZ -> YXZ:  info.num_samples info.r_cols fft_size  -> info.r_cols info.num_samples  fft_size
	launch_size = cuda::make_launch_config(cuda::grid::dimensions_t((int)info.num_samples/TILE_DIM+1,(int) info.r_cols/TILE_DIM+1, fft_size/TILE_DIM+1), cuda::grid::dimensions_t(TILE_DIM,TILE_DIM,TILE_DIM));
	current_dev.launch(ArcaWave::Radar::CUDA::transposeYXZ<T, V>,
	    launch_size,
		(thrust::raw_pointer_cast(d_azimuth_out.data())),(thrust::raw_pointer_cast(d_range_padding.data())),(int)info.num_samples,(int) info.r_cols,fft_size
	    );
	
	current_dev.synchronize();

	// Zero Padding on first axis: info.r_cols info.num_samples  fft_size -> fft_size info.num_samples  fft_size
	launch_size = cuda::make_launch_config(cuda::grid::dimensions_t( fft_size/TILE_DIM+1,(int)info.num_samples/TILE_DIM+1, fft_size/TILE_DIM+1), cuda::grid::dimensions_t(TILE_DIM,TILE_DIM,TILE_DIM));
	current_dev.launch(ArcaWave::Radar::CUDA::zeroPadding<T, V>,
	    launch_size,
		(thrust::raw_pointer_cast(d_azimuth_padding.data())),(thrust::raw_pointer_cast(d_azimuth_out.data())),(int) info.r_cols,(int)info.num_samples,fft_size,fft_size
	    );
	current_dev.synchronize();





	// Elevation FFT : 
	// fft_size info.num_samples fft_size -> fft_size info.num_samples fft_size
	CufftProcessing(fft_size ,(int)info.num_samples*fft_size,d_azimuth_padding,d_elevation_out);
	current_dev.synchronize();

	// Transpose YXZ -> XYZ: fft_size info.num_samples fft_size -> info.num_samples fft_size fft_size
	launch_size = cuda::make_launch_config(cuda::grid::dimensions_t(fft_size/TILE_DIM+1,(int)info.num_samples/TILE_DIM+1, fft_size/TILE_DIM+1), cuda::grid::dimensions_t(TILE_DIM,TILE_DIM,TILE_DIM));
	current_dev.launch(ArcaWave::Radar::CUDA::transposeYXZ<T, V>,
	    launch_size,
		(thrust::raw_pointer_cast(d_azimuth_padding.data())),(thrust::raw_pointer_cast(d_elevation_out.data())),fft_size,(int)info.num_samples,fft_size
	    );
	current_dev.synchronize();

	// Last fftshift: info.num_samples fft_size fft_size
	launch_size = cuda::make_launch_config(cuda::grid::dimensions_t((int)info.num_samples/TILE_DIM+1,fft_size/TILE_DIM+1, fft_size/TILE_DIM+1), cuda::grid::dimensions_t(TILE_DIM,TILE_DIM,TILE_DIM));
	current_dev.launch(ArcaWave::Radar::CUDA::fftShift<T, V>,
	    launch_size,
		(thrust::raw_pointer_cast(d_data_out.data())),(thrust::raw_pointer_cast(d_azimuth_padding.data())),(int)info.num_samples,fft_size,fft_size
	    );
	current_dev.synchronize();	



	// Thresholding	
	ThresholdProcessing(output_size,d_data_out);
	current_dev.synchronize();	


	FFTOutput.reserve(output_size);
	thrust::copy(d_data_out.begin(), d_data_out.end(), FFTOutput.begin());

}

template<typename T, uint_fast32_t V>
void CUDAFourierTransform<T, V>::CufftProcessing(int fftsize, int batches,thrust::device_vector<complex<T>>& input,thrust::device_vector<complex<T>>& output )
{

	cufftResult cuRet ;
	cufftHandle plan;
	int rank = 1;                           // --- 1D FFTs
    int n[] = {fftsize};                        // --- Size of the Fourier transform
    int istride = 1, ostride = 1;           // --- Distance between two successive input/output elements
    int idist =fftsize, odist = fftsize;               // --- Distance between batches
    int inembed[] = {0};                  	// --- Input size with pitch (ignored for 1D transforms) : input[b * idist + ((x * inembed[1] + y) * inembed[2] + z) * istride]
    int onembed[] = {0};                  	// --- Output size with pitch (ignored for 1D transforms) : output[b * odist + ((x * onembed[1] + y) * onembed[2] + z) * ostride]
    int batch = batches;                          // --- Number of batched executions

	if constexpr (std::is_same<T,float>::value){
		cuRet = cufftPlanMany(&plan, rank, n, 
                  inembed, istride, idist,
                  onembed, ostride, odist, CUFFT_C2C, batch);

		if (CUFFT_SUCCESS != cuRet)
		{
			cout<<"Failed in plan creation"<<endl ;
		}
		cuRet = cufftExecC2C(plan, (cufftComplex *)(thrust::raw_pointer_cast(input.data())), (cufftComplex *)(thrust::raw_pointer_cast(output.data())),CUFFT_FORWARD);
		if (CUFFT_SUCCESS != cuRet)
		{
			cout<<"Failed in plan exec"<<endl ;

		}
	}else{
		cuRet = cufftPlanMany(&plan, rank, n, 
                  inembed, istride, idist,
                  onembed, ostride, odist, CUFFT_Z2Z, batch);

		if (CUFFT_SUCCESS != cuRet)
		{
			cout<<"Failed in plan creation"<<endl ;
		}
		cuRet = cufftExecZ2Z(plan, (cufftDoubleComplex *)(thrust::raw_pointer_cast(input.data())), (cufftDoubleComplex *)(thrust::raw_pointer_cast(output.data())),CUFFT_FORWARD);
		if (CUFFT_SUCCESS != cuRet)
		{
			cout<<"Failed in plan exec"<<endl ;

		}
	}
    

    cufftDestroy(plan);

}

template<typename T, uint_fast32_t V>
void CUDAFourierTransform<T, V>::ThresholdProcessing(int size, thrust::device_vector<complex<T>>& input )
{
	thrust::device_vector<T> d_threshold_out(size);
	thrust::device_vector<T> d_threshold_out2(size);

	T max, min; 
	// compute absolute value
	thrust::transform(input.begin(), input.end(), d_threshold_out.begin(), ArcaWave::Radar::complex_abs<T,V> ());

	max = *(thrust::max_element(d_threshold_out.begin(), d_threshold_out.end()));
	min = *(thrust::min_element(d_threshold_out.begin(), d_threshold_out.end()));
	auto threshold = (max-min)/6.0+min;
	
	// perform thresholding
	thrust::transform(d_threshold_out.begin(), d_threshold_out.end(), d_threshold_out2.begin(), ArcaWave::Radar::threshold_f<T,V> (threshold));

	ThresholdedOutput.reserve(size);
	thrust::copy(d_threshold_out2.begin(), d_threshold_out2.end(), ThresholdedOutput.begin());
}

template<typename T, uint_fast32_t V>
void CUDAFourierTransform<T, V>::SaveResult(string fname)
{
	// cnpy::npy_save(fname,&FFTOutput[0],{fft_size,fft_size,num_samples},"w");
}

template<typename T, uint_fast32_t V>
void CUDAFourierTransform<T, V>::SaveThresholdedResult(string fname)
{
	// cnpy::npy_save(fname,&ThresholdedOutput[0],{fft_size,fft_size,num_samples},"w");
}

// Explicitly defines templated instances
template class ArcaWave::Radar::CUDAFourierTransform<float, 3>;
template class ArcaWave::Radar::CUDAFourierTransform<double, 3>;
