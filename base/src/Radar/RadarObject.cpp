
#include "Radar/RadarObject.h"

// STL includes
#include <fstream>
#include <iostream>
#include <string>

// JSON include
#include <nlohmann/json.hpp>

// ArcaWave includes
#include "Geometry/RayTracing/CPURayTracer.h"
#include "Radar/UniformDiscretizer.h"
#include "Utility/Constants.h"
#include "Radar/Calculator/PO.h"

using namespace std;
using namespace std::filesystem;
using namespace nlohmann;
using namespace ArcaWave::Geometry;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
RadarObject<T, V>::RadarObject(const std::shared_ptr<Object<T, V>>& Obj) :
	Object<T, V>(*Obj)
{}

template<typename T, uint_fast32_t V>
RadarObject<T, V>::RadarObject(const shared_ptr<RadarObjectCreateInfo<T, V>>& Info) :
	Object<T, V>(MakePartitions(Info))
{
	// Gets the expanded list of partitions
	vector<pair<shared_ptr<Partition<T, V>>, uint_fast32_t>> expanded = ExpandPartitions(Info);

	// Goes through each of the new partitions to count the number of VCMs and organize the list
	this->Partitions = shared_ptr<shared_ptr<Partition<T, V>>[]>(new shared_ptr<Partition<T, V>>[expanded.size()]);
	int_fast8_t vcm_count = 0;
	for(uint_fast32_t i = 0; i < expanded.size(); ++i)
	{
		this->Partitions[i] = get<0>(expanded[i]);
		if(static_cast<int_fast8_t>(this->Partitions[i]->Index.Index) >= vcm_count)
			vcm_count = static_cast<int_fast8_t>(this->Partitions[i]->Index.Index) + 1;
	}

	// Initializes the list of VCMs
	this->VCMCount = static_cast<uint_fast32_t>(vcm_count);
	this->VCMSet = shared_ptr<shared_ptr<VCM<T, V>>[]>(new shared_ptr<VCM<T, V>>[this->VCMCount]);
}

template<typename T, uint_fast32_t V>
RadarObject<T, V>::~RadarObject()
{}

template<typename T, uint_fast32_t V>
shared_ptr<Partition<T, V>> RadarObject<T, V>::GetPartition(uint_fast32_t Index)
{
	return Partitions[Index];
}

// template<typename T, uint_fast32_t V>
// std::shared_ptr<VCM<T, V>> RadarObject<T, V>::GetFaceVCM(uint_fast32_t MeshIndex, uint_fast32_t FaceIndex)
// {
//     return VCMSet[0];
// 	auto const& face = Object<T, V>::GetMesh(MeshIndex)->GetFace(FaceIndex);
// 	auto face_vertices = std::vector<Eigen::Matrix<T, V, 1>>
// 	{
// 		Object<T, V>::GetMesh(MeshIndex)->GetVertex(face.A),
// 		Object<T, V>::GetMesh(MeshIndex)->GetVertex(face.B),
// 		Object<T, V>::GetMesh(MeshIndex)->GetVertex(face.C)
// 	};

// 	for (uint_fast32_t i = 0; i < SubBoundingBoxes.size(); i++)
// 		if ( SubBoundingBoxes[i].NumPointsInAABB(face_vertices) == 3 ) 
// 			return VCMSet[i];

// 	return nullptr;
// }

template<typename T, uint_fast32_t V>
void RadarObject<T, V>::Bake(const std::shared_ptr<VCMBuildInfo<T, V>>& BuildInfo, uint_fast32_t ThreadCount)
{
	// Figures out which meshes to use to bake the VCMs
	shared_ptr<uint_fast32_t[]> bake_meshes = shared_ptr<uint_fast32_t[]>(new uint_fast32_t[this->VCMCount]);
	for(uint_fast32_t i = 0; i < this->VCMCount; ++i)
	{
		bool vcm_found = false;
		for(uint_fast32_t j = 0; j < this->MeshCount; ++j)
		{
			VCMIndex idx = this->Partitions[j]->Index;
			if(static_cast<uint_fast32_t>(idx.Index) == i && !idx.XFlip && !idx.YFlip && !idx.ZFlip)
			{
				bake_meshes[i] = j;
				vcm_found = true;
				break;
			}
		}
		if(!vcm_found)
			cout << "VCM NOT FOUND" << endl;
	}

	// Creates sub-objects for each VCM 
	shared_ptr<shared_ptr<Object<T, V>>[]> bake_objs = shared_ptr<shared_ptr<Object<T, V>>[]>(new shared_ptr<Object<T, V>>[this->VCMCount]);
	for(uint_fast32_t i = 0; i < this->VCMCount; ++i)
	{
		// Gets the mesh to bake a VCM on and copies it
		shared_ptr<Mesh<T, V>> mesh = this->Meshes[bake_meshes[i]];
		shared_ptr<Mesh<T, V>> bake_mesh = mesh->BuildSubmesh(mesh->GetFullSubmesh());

		// Translates each mesh so that it is in the center
		Matrix<T, V, 1> trans = this->Partitions[bake_meshes[i]]->Vertex;
		for(uint_fast32_t j = 0; j < bake_mesh->GetVertexCount(); ++j)
		{
			Matrix<T, V, 1> vert = bake_mesh->GetVertex(j);
			bake_mesh->Vertices[j] = vert - trans;
		}

		// Builds a new object for the mesh to bake and adds it to the list
		shared_ptr<ObjectCreateInfo<T, V>> obj_info = make_shared<ObjectCreateInfo<T, V>>();
		obj_info->ObjectName = "";
		obj_info->MeshCount = 1;
		obj_info->Meshes = shared_ptr<shared_ptr<Mesh<T, V>>[]>(new shared_ptr<Mesh<T, V>>[1]);
		obj_info->Meshes[0] = bake_mesh;
		obj_info->BoundingBox = bake_mesh->BoundingBox;
		bake_objs[i] = make_shared<Object<T, V>>(obj_info);
	}

	// Creates the thread pool
    shared_ptr<ThreadPool> thrd_pool = make_shared<ThreadPool>(ThreadCount, 1);

    // Creates the environment and places the single object within it
    shared_ptr<Environment<T, V>> full_env = make_shared<Environment<T, V>>();
    shared_ptr<Instance<T, V>> full_inst = make_shared<Instance<T, V>>(0, 0);
	shared_ptr<ObjectCreateInfo<T, V>> full_obj_info = make_shared<ObjectCreateInfo<T, V>>();
	full_obj_info->ObjectName = "";
	full_obj_info->MeshCount = this->MeshCount;
	full_obj_info->Meshes = this->Meshes;
	full_obj_info->BoundingBox = this->BoundingBox;
	shared_ptr<Object<T, V>> full_obj = make_shared<Object<T, V>>(full_obj_info);
    full_env->AddObject(full_obj);
    full_env->AddInstance(full_inst);

    // Makes the full ray tracer
    shared_ptr<RayTracer<T, V>> full_rtx = make_shared<CPURayTracer<T, V>>(full_env);

    // Makes the discretizer
    shared_ptr<Discretizer<T, V>> disc = make_shared<UniformDiscretizer<T, V>>();

    // Makes the calculator
    shared_ptr<CurrentCalculator<T, V>> calc = make_shared<PO<T, V>>();

	// Bakes each VCM
	for(uint_fast32_t i = 0; i < this->VCMCount; ++i)
	{
		cout << "[BAKING VCM] " << i << "/" << this->VCMCount << endl;

		// Creates the raytracer for the specific baked object
		shared_ptr<Environment<T, V>> obj_env = make_shared<Environment<T, V>>();
		shared_ptr<Instance<T, V>> obj_inst = make_shared<Instance<T, V>>(0, 0);
		obj_env->AddObject(bake_objs[i]);
		obj_env->AddInstance(obj_inst);
		shared_ptr<RayTracer<T, V>> obj_rtx = make_shared<CPURayTracer<T, V>>(obj_env);

		// Creates the bake info
		shared_ptr<VCMBakeInfo<T, V>> bake_info = make_shared<VCMBakeInfo<T, V>>();
		bake_info->Build 				= BuildInfo;
		bake_info->Obj					= bake_objs[i];
		bake_info->ObjTracer 			= obj_rtx;
		bake_info->FullTracer			= full_rtx;
		bake_info->Disc					= disc;
		bake_info->ThrdPool				= thrd_pool;
		bake_info->CurrentCalculator	= calc;

		// Bakes the VCM
		this->VCMSet[i] = VCM<T, V>::Bake(bake_info);
	}
}

template<typename T, uint_fast32_t V>
void RadarObject<T, V>::Export(const path& Path, const shared_ptr<RadarObject<T, V>>& RadObj)
{
	const std::string OBJ_EXTENSION		= ".obj";
	const std::string JSON_EXTENSION	= ".json";
	const std::string MODEL_FILE_NAME 	= "model_file";
	const std::string PART_ARRAY		= "partition_array";
	const std::string PART_VERT			= "vertex";
	const std::string PART_WEIGHT		= "weight";
	const std::string PART_ENABLED		= "enabled";
	const std::string PART_INDEX		= "vcm_index";
	const std::string PART_X_FLIP		= "x_flip";
	const std::string PART_Y_FLIP		= "y_flip";
	const std::string PART_Z_FLIP		= "z_flip";
	const std::string VCM_COUNT 		= "vcm_count";
	const std::string VCM_ARRAY			= "vcm_array";
	const std::string VCM_FREQ			= "frequency";
	const std::string VCM_POLAR			= "polarization";
	const std::string VCM_INC_THETA		= "inc_theta";
	const std::string VCM_INC_PHI		= "inc_phi";
	const std::string VCM_REF_THETA		= "ref_theta";
	const std::string VCM_REF_PHI		= "ref_phi";
	const std::string VCM_MOMENTS		= "moments";
	const std::string VCM_MOMENT_ENTRY	= "moment";

	// Exports the model
	json radar_object_json;
	radar_object_json[MODEL_FILE_NAME] = Path.string().append(OBJ_EXTENSION);
	cout << "Export " << RadObj->GetMeshCount() << endl;
	for(uint_fast32_t i = 0; i < RadObj->GetMeshCount(); ++i)
		RadObj->GetMesh(i)->MaterialInfo.MaterialColor = {static_cast<T>(i)/RadObj->GetMeshCount(), static_cast<T>(i)/RadObj->GetMeshCount(), static_cast<T>(i)/RadObj->GetMeshCount()};
	Object<T, V>::Export(RadObj, static_cast<string>(radar_object_json[MODEL_FILE_NAME]), EX_OBJ);

	// Exports the partitions
	radar_object_json[PART_ARRAY] = json::array();
	for(uint_fast32_t i = 0; i < RadObj->GetMeshCount(); ++i)
	{
		// Creates the JSON entry for the partition
		json part_json;
		part_json[PART_VERT] = json::array(
		{
			RadObj->Partitions[i]->Vertex.x(), 
			RadObj->Partitions[i]->Vertex.y(), 
			RadObj->Partitions[i]->Vertex.z()
		});
		part_json[PART_WEIGHT] = RadObj->Partitions[i]->Weight;
		part_json[PART_ENABLED] = RadObj->Partitions[i]->Enabled;
		part_json[PART_INDEX] = RadObj->Partitions[i]->Index.Index;
		part_json[PART_X_FLIP] = RadObj->Partitions[i]->Index.XFlip;
		part_json[PART_Y_FLIP] = RadObj->Partitions[i]->Index.YFlip;
		part_json[PART_Z_FLIP] = RadObj->Partitions[i]->Index.ZFlip;

		// Adds the partition entry
		radar_object_json[PART_ARRAY].push_back(part_json);
	}

	// Exports each VCM
	radar_object_json[VCM_COUNT] = RadObj->VCMCount;
	radar_object_json[VCM_ARRAY] = json::array();
	for(uint_fast32_t i = 0; i < RadObj->VCMCount; ++i)
	{
		// Gets each VCM
		shared_ptr<VCM<T, V>> vcm = RadObj->VCMSet[i];

		// Builds the JSON entry for the VCM
		json vcm_json;
		vcm_json[VCM_FREQ] = vcm->Build->Frequency;
		vcm_json[VCM_POLAR] = json::array(
		{
			vcm->Build->Polarization.x().real(),
			vcm->Build->Polarization.x().imag(),
			vcm->Build->Polarization.y().real(),
			vcm->Build->Polarization.x().imag()
		});
		vcm_json[VCM_INC_THETA] = json::array(
		{
			vcm->Build->IncThetaInterval.Size,
			vcm->Build->IncThetaInterval.Step,
			vcm->Build->IncThetaInterval.Offset
		});
		vcm_json[VCM_INC_PHI] = json::array(
		{
			vcm->Build->IncPhiInterval.Size,
			vcm->Build->IncPhiInterval.Step,
			vcm->Build->IncPhiInterval.Offset
		});
		vcm_json[VCM_REF_THETA] = json::array(
		{
			vcm->Build->RefThetaInterval.Size,
			vcm->Build->RefThetaInterval.Step,
			vcm->Build->RefThetaInterval.Offset
		});
		vcm_json[VCM_REF_PHI] = json::array(
		{
			vcm->Build->RefPhiInterval.Size,
			vcm->Build->RefPhiInterval.Step,
			vcm->Build->RefPhiInterval.Offset
		});

		// Copies the entire VCM
		vcm_json[VCM_MOMENTS] = json::array();
		uint_fast32_t total_size = vcm->Build->IncThetaInterval.Size * vcm->Build->IncPhiInterval.Size * vcm->Build->RefThetaInterval.Size * vcm->Build->RefPhiInterval.Size;
		for(uint_fast32_t j = 0; j < total_size; ++j)
		{
			// Creates each moment of the VCM
			json moment_json;
			moment_json[VCM_MOMENT_ENTRY] = json::array(
			{
				vcm->Moments[j].x().real(),
				vcm->Moments[j].x().imag(),
				vcm->Moments[j].y().real(),
				vcm->Moments[j].y().imag(),
				vcm->Moments[j].z().real(),
				vcm->Moments[j].z().imag()
			});

			// Adds the moment to the moment list
			vcm_json[VCM_MOMENTS].push_back(moment_json);
		}

		// Adds the JSON entry for the VCM to the file
		radar_object_json[VCM_ARRAY].push_back(vcm_json);
	}

	// Exports the JSON
	cout << Path.string().append(JSON_EXTENSION) << endl;
	std::ofstream(Path.string().append(JSON_EXTENSION)) << radar_object_json;
}

template<typename T, uint_fast32_t V>
shared_ptr<RadarObject<T, V>> RadarObject<T, V>::Import(const path& Path)
{
	const std::string OBJ_EXTENSION		= ".obj";
	const std::string JSON_EXTENSION	= ".json";
	const std::string MODEL_FILE_NAME 	= "model_file";
	const std::string PART_ARRAY		= "partition_array";
	const std::string PART_VERT			= "vertex";
	const std::string PART_WEIGHT		= "weight";
	const std::string PART_ENABLED		= "enabled";
	const std::string PART_INDEX		= "vcm_index";
	const std::string PART_X_FLIP		= "x_flip";
	const std::string PART_Y_FLIP		= "y_flip";
	const std::string PART_Z_FLIP		= "z_flip";
	const std::string VCM_COUNT 		= "vcm_count";
	const std::string VCM_ARRAY			= "vcm_array";
	const std::string VCM_FREQ			= "frequency";
	const std::string VCM_POLAR			= "polarization";
	const std::string VCM_INC_THETA		= "inc_theta";
	const std::string VCM_INC_PHI		= "inc_phi";
	const std::string VCM_REF_THETA		= "ref_theta";
	const std::string VCM_REF_PHI		= "ref_phi";
	const std::string VCM_MOMENTS		= "moments";
	const std::string VCM_MOMENT_ENTRY	= "moment";

	// Gets the JSON
	json radar_object_json;
    std::cout << Path.string().append(JSON_EXTENSION) << std::endl;
    std::ifstream(Path.string().append(JSON_EXTENSION)) >> radar_object_json;
	// Imports the object and creates the radar object
	auto path_to_obj = Path.parent_path().append(static_cast<string>(radar_object_json[MODEL_FILE_NAME]));
	shared_ptr<Object<T, V>> obj = Object<T, V>::Import(path_to_obj.string());
	shared_ptr<RadarObject<T, V>> rad_obj = make_shared<RadarObject<T, V>>(obj);

	// Copies all of the partitions over
	uint_fast32_t mesh_count = rad_obj->GetMeshCount();
	rad_obj->Partitions = shared_ptr<shared_ptr<Partition<T, V>>[]>(new shared_ptr<Partition<T, V>>[mesh_count]);
	for(uint_fast32_t i = 0; i < mesh_count; ++i)
	{
		// Gets each partition JSON
		json part_json = radar_object_json[PART_ARRAY][i];

		// Gets the partition fields
		Matrix<T, V, 1> part_vert(part_json[PART_VERT][0], part_json[PART_VERT][1], part_json[PART_VERT][2]);
		T part_weight = part_json[PART_WEIGHT];
		bool part_enabled = part_json[PART_ENABLED];
		int_fast32_t index = part_json[PART_INDEX];
		bool x_flip = part_json[PART_X_FLIP];
		bool y_flip = part_json[PART_Y_FLIP];
		bool z_flip = part_json[PART_Z_FLIP];

		// Sets the partition index, and places it in the radar object
		shared_ptr<Partition<T, V>> part = make_shared<Partition<T, V>>(part_vert, part_weight, part_enabled);
		part->Index = {index, x_flip, y_flip, z_flip};
		rad_obj->Partitions[i] = part;
	}

	// Copies all of the VCM data over
	uint_fast32_t vcm_count = radar_object_json[VCM_COUNT];
	rad_obj->VCMCount = vcm_count;
    rad_obj->VCMSet = shared_ptr<shared_ptr<VCM<T, V>>[]>(new shared_ptr<VCM<T, V>>[rad_obj->VCMCount]);

	for(uint_fast32_t i = 0; i < vcm_count; ++i)
	{
		// Gets each VCM JSON
		json vcm_json = radar_object_json[VCM_ARRAY][i];

		// Creates the VCM build information and builds the VCM (converts degrees to radians)
		shared_ptr<VCMBuildInfo<T, V>> vcm_build_info = make_shared<VCMBuildInfo<T, V>>();
		vcm_build_info->Mode = VCM_BISTATIC;
		vcm_build_info->Frequency = vcm_json[VCM_FREQ];
		vcm_build_info->Polarization = Matrix<complex<T>, 2, 1>
		(
			complex<T>(vcm_json[VCM_POLAR][0], vcm_json[VCM_POLAR][1]),
			complex<T>(vcm_json[VCM_POLAR][2], vcm_json[VCM_POLAR][3])
		);
		vcm_build_info->IncThetaInterval = Interval<T>(vcm_json[VCM_INC_THETA][0], static_cast<T>(vcm_json[VCM_INC_THETA][1]) * PI<T> / 180, static_cast<T>(vcm_json[VCM_INC_THETA][2]) * PI<T> / 180);
		vcm_build_info->IncPhiInterval = Interval<T>(vcm_json[VCM_INC_PHI][0], static_cast<T>(vcm_json[VCM_INC_PHI][1]) * PI<T> / 180, static_cast<T>(vcm_json[VCM_INC_PHI][2]) * PI<T> / 180);
		vcm_build_info->RefThetaInterval = Interval<T>(vcm_json[VCM_REF_THETA][0], static_cast<T>(vcm_json[VCM_REF_THETA][1]) * PI<T> / 180, static_cast<T>(vcm_json[VCM_REF_THETA][2]) * PI<T> / 180);
		vcm_build_info->RefPhiInterval = Interval<T>(vcm_json[VCM_REF_PHI][0], static_cast<T>(vcm_json[VCM_REF_PHI][1]) * PI<T> / 180, static_cast<T>(vcm_json[VCM_REF_PHI][2]) * PI<T> / 180);
		shared_ptr<VCM<T, V>> vcm = make_shared<VCM<T, V>>(vcm_build_info);

		// Copies all of the moment data for each VCM
		uint_fast32_t mom_size = vcm_build_info->IncThetaInterval.Size * vcm_build_info->IncPhiInterval.Size * vcm_build_info->RefThetaInterval.Size * vcm_build_info->RefPhiInterval.Size;
		for(uint_fast32_t j = 0; j < mom_size; ++j)
		{
			// Gets the JSON entry for each moment
			json moment_json = vcm_json[VCM_MOMENTS][j];

			// Creates the moment and adds it to the VCM
			Matrix<complex<T>, V, 1> moment
			(
				complex<T>(moment_json[VCM_MOMENT_ENTRY][0], moment_json[VCM_MOMENT_ENTRY][1]),
				complex<T>(moment_json[VCM_MOMENT_ENTRY][2], moment_json[VCM_MOMENT_ENTRY][3]),
				complex<T>(moment_json[VCM_MOMENT_ENTRY][4], moment_json[VCM_MOMENT_ENTRY][5])
			);
			vcm->Moments[j] = moment;
		}

		// Adds the VCM to the radar object
		rad_obj->VCMSet[i] = vcm;
	}

	// Returns the radar object
	return rad_obj;
}

template<typename T, uint_fast32_t V>
vector<pair<shared_ptr<Partition<T, V>>, uint_fast32_t>> RadarObject<T, V>::ExpandPartitions(const shared_ptr<RadarObjectCreateInfo<T, V>>& Info)
{
	// Creates the expanded list of partitions
	vector<pair<shared_ptr<Partition<T, V>>, uint_fast32_t>> partition_vector;
	int_fast8_t vcm_index = 0;
	for(uint_fast32_t i = 0; i < Info->PartitionCount; ++i)
	{
		// Gets the mesh index of the partition
		uint_fast32_t mesh_idx = Info->MeshIndicies[i];

		// Makes a copy of the original partition
		const T x = Info->Partitions[i]->Vertex.x();
        const T y = Info->Partitions[i]->Vertex.y();
        const T z = Info->Partitions[i]->Vertex.z();
        const T weight = Info->Partitions[i]->Weight;
        const bool enabled = Info->Partitions[i]->Enabled;
		shared_ptr<Partition<T, V>> part = make_shared<Partition<T, V>>(Matrix<T, V, 1>(x, y, z), weight, enabled);

		// Copies the normal partition while setting the VCM Index
		part->Index = {enabled ? vcm_index : static_cast<int_fast8_t>(-1), false, false, false};
		partition_vector.push_back(make_pair(part, mesh_idx));

		// Takes into account x-symmetry
		if((Info->XSym && x > FLOATING_POINT_ERROR<T>))
		{
			shared_ptr<Partition<T, V>> sym_part = make_shared<Partition<T, V>>(Matrix<T, V, 1>(-x, y, z), weight, enabled);
			sym_part->Index = {enabled ? vcm_index : static_cast<int_fast8_t>(-1), true, false, false};
			partition_vector.push_back(make_pair(sym_part, mesh_idx));
		}

		// Takes into account y-symmetry
		if((Info->YSym && y > FLOATING_POINT_ERROR<T>))
		{
			shared_ptr<Partition<T, V>> sym_part = make_shared<Partition<T, V>>(Matrix<T, V, 1>(x, -y, z), weight, enabled);
			sym_part->Index = {enabled ? vcm_index : static_cast<int_fast8_t>(-1), false, true, false};
			partition_vector.push_back(make_pair(sym_part, mesh_idx));
		}

		// Takes into account only z-symmetry
		if((Info->ZSym && z > FLOATING_POINT_ERROR<T>))
		{
			shared_ptr<Partition<T, V>> sym_part = make_shared<Partition<T, V>>(Matrix<T, V, 1>(x, y, -z), weight, enabled);
			sym_part->Index = {enabled ? vcm_index : static_cast<int_fast8_t>(-1), false, false, true};
			partition_vector.push_back(make_pair(sym_part, mesh_idx));
		}

		// Takes into account xy-symmetry
		if((Info->XSym && x > FLOATING_POINT_ERROR<T>) && (Info->YSym && y > FLOATING_POINT_ERROR<T>))
		{
			shared_ptr<Partition<T, V>> sym_part = make_shared<Partition<T, V>>(Matrix<T, V, 1>(-x, -y, z), weight, enabled);
			sym_part->Index = {enabled ? vcm_index : static_cast<int_fast8_t>(-1), true, true, false};
			partition_vector.push_back(make_pair(sym_part, mesh_idx));
		}

		// Takes into account xz-symmetry
		if((Info->XSym && x > FLOATING_POINT_ERROR<T>) && (Info->ZSym && z > FLOATING_POINT_ERROR<T>))
		{
			shared_ptr<Partition<T, V>> sym_part = make_shared<Partition<T, V>>(Matrix<T, V, 1>(-x, y, -z), weight, enabled);
			sym_part->Index = {enabled ? vcm_index : static_cast<int_fast8_t>(-1), true, false, true};
			partition_vector.push_back(make_pair(sym_part, mesh_idx));
		}

		// Takes into account yz-symmetry
		if(!(Info->XSym) && (Info->YSym && y > FLOATING_POINT_ERROR<T>) && (Info->ZSym && z > FLOATING_POINT_ERROR<T>))
		{
			shared_ptr<Partition<T, V>> sym_part = make_shared<Partition<T, V>>(Matrix<T, V, 1>(x, -y, -z), weight, enabled);
			sym_part->Index = {enabled ? vcm_index : static_cast<int_fast8_t>(-1), false, true, true};
			partition_vector.push_back(make_pair(sym_part, mesh_idx));
		}

		// Takes into account xyz-symmetry
		if((Info->XSym && x > FLOATING_POINT_ERROR<T>) && (Info->YSym && y > FLOATING_POINT_ERROR<T>) && (Info->ZSym && z > FLOATING_POINT_ERROR<T>))
		{
			shared_ptr<Partition<T, V>> sym_part = make_shared<Partition<T, V>>(Matrix<T, V, 1>(-x, -y, -z), weight, enabled);
			sym_part->Index = {enabled ? vcm_index : static_cast<int_fast8_t>(-1), true, true, true};
			partition_vector.push_back(make_pair(sym_part, mesh_idx));
		}

		// Increments the VCM index (if enabled)
		if(enabled)
			++vcm_index;
	}

	// Returns the new vector of partitions
	return partition_vector;
}

template<typename T, uint_fast32_t V>
shared_ptr<ObjectCreateInfo<T, V>> RadarObject<T, V>::MakePartitions(const shared_ptr<RadarObjectCreateInfo<T, V>>& Info)
{
	// Gets the expanded list of partitions
	vector<pair<shared_ptr<Partition<T, V>>, uint_fast32_t>> expanded = ExpandPartitions(Info);

	// Initializes the creation info for a new partition object.
    shared_ptr<ObjectCreateInfo<T, V>> obj_info = make_shared<ObjectCreateInfo<T, V>>();
    obj_info->ObjectName = "";
    obj_info->MeshCount = expanded.size();
    obj_info->Meshes = shared_ptr<shared_ptr<Mesh<T, V>>[]>(new shared_ptr<Mesh<T, V>>[expanded.size()]);
    obj_info->BoundingBox = Info->Obj->GetBoundingBox();

	// Creates a new mesh for each partitions
    uint_fast32_t index = 0;
    for(uint_fast32_t i = 0; i < Info->Obj->GetMeshCount(); ++i)
    {
        // Creates the partitions of the mesh
        const shared_ptr<Mesh<T, V>> mesh = Info->Obj->GetMesh(i);
		
		// Only consider the partitions for the given mesh
		vector<shared_ptr<Partition<T, V>>> mesh_parts;
		for(uint_fast32_t j = 0; j < expanded.size(); ++j)
			if(get<1>(expanded[j]) == i)
				mesh_parts.push_back(get<0>(expanded[j]));

        // Creates the list of submeshes which define the partitions
		shared_ptr<shared_ptr<Submesh>[]> submeshes = shared_ptr<shared_ptr<Submesh>[]>(new shared_ptr<Submesh>[mesh_parts.size()]);
		for(uint_fast32_t j = 0; j < mesh_parts.size(); ++j)
			submeshes[j] = make_shared<Submesh>();

		// Goes through each face in the mesh and assigns it to a partition
        for(uint_fast32_t j = 0; j < mesh->GetFaceCount(); ++j)
        {
            // Gets the center of each face
            const Triangle<T, V> t = mesh->GetTriangle(j);
            const Matrix<T, V, 1> vert = t.Center();

            // Finds the partition that minimizes the distance to the center of the face
            uint_fast32_t min_idx = 0;
            Matrix<T, V, 1> min_vert = mesh_parts[0]->Vertex;
            T min_weight = mesh_parts[0]->Weight;
            for(uint_fast32_t k = 1; k < mesh_parts.size(); ++k)
            {
                const Matrix<T, V, 1> part_vert = mesh_parts[k]->Vertex;
                const T part_weight = mesh_parts[k]->Weight;
                if(((part_vert - vert).norm() / part_weight) < ((min_vert - vert).norm() / min_weight))
                {
                    min_idx = k;
                    min_vert = part_vert;
                    min_weight = part_weight;
                }
            }

            // Adds the face to one of the partitions
            submeshes[min_idx]->AddFace(j);
        }

        // Builds each of the submeshes and adds it to the creation info
        for(uint_fast32_t j = 0; j < mesh_parts.size(); ++j)
        {
            obj_info->Meshes[index] = mesh->BuildSubmesh(submeshes[j]);
            ++index;
        }
    }

	// Returns the creation info
	return obj_info;
}

template class ArcaWave::Radar::RadarObject<float, 3>;
template class ArcaWave::Radar::RadarObject<double, 3>;
