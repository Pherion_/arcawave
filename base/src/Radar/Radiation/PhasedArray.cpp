
#include <cstdint>
#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include "Radar/Radiation/PhasedArray.h"
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V> 
PhasedArray<T, V>::PhasedArray(Matrix<T, 2, 1> BeamDirection, shared_ptr<vector<shared_ptr<RadiationField<T, V>>>> Transmitters, 
                                T Phase, uint_fast32_t Row, uint_fast32_t Column) :
    BeamDirection(BeamDirection), Transmitters(Transmitters), Phase(PI<T>/2), Row(Row), Column(Column)
{}



template<typename T, uint_fast32_t V>
Matrix<complex<T>, V, 1> PhasedArray<T, V>:: ElectricField(T Time, const Eigen::Matrix<T, V, 1>& Point)
{
    // Beam direction
    T phi_0 = BeamDirection.x();
    T theta_0 = BeamDirection.y();

    // calculate the spherical coordinate of the point
    T r = Point.norm();
    T phi = acos(Point.z()/ r);
    T theta = atan(Point.y()/Point.x());
    Matrix<complex<T>, V, 1> e(0,0,0);

    // calculate the E field by summing together different contrinutions from each tx due to phase differences with respective to this point 
    for(uint_fast32_t j = 0; j < Column; j++)
    {
        shared_ptr<RadiationField<T, V>> t = Transmitters->at(j);
        Matrix<T, V, 1> t_position = t->Source;

        // find the spherical coordinates of the tx position
        T r_i = t_position.norm();
        T theta_i = atan(t_position.y()/t_position.x());    
        T psi_i = sin(phi)*cos(theta_i - theta) - sin(phi_0)*cos(theta_i - theta_0);

        T k = 2 * PI<T> * t->Frequency(Time, Point) / C_0<T>;

        // CurrentIn will return the contrinutions of tx in this column to eliminate back lobe
        Matrix<complex<T>, V, 1> ex = CurrentIn(Time, Point, j) * exp(I<T> * k * r_i * psi_i);
        e += ex;
    }

    return e;
}


template<typename T , uint_fast32_t V>
void PhasedArray<T, V>::resetBeamDirection(Eigen::Matrix<T, 2, 1> beam)
{
        BeamDirection = beam;
}


template<typename T , uint_fast32_t V>
Matrix<complex<T>, V, 1> PhasedArray<T, V>::CurrentIn(T Time, const Eigen::Matrix<T, V, 1>& Point, uint_fast32_t Column_i)
{
    Matrix<complex<T>, V, 1> current(0.0, 0.0, 0.0);
    for(uint_fast32_t n = 0; n < Row; n++)
    {
        shared_ptr<RadiationField<T, V>> t = Transmitters->at(n * Row + Column_i);

        // adding a phase to each of the following tx in this column then sum 
        current += t->ElectricField(Time, t->Source) * exp( I<T> * T(n) * Phase);
    }
    return current;
}



template<typename T, uint_fast32_t V>
PhasedArray<T, V>& PhasedArray<T, V>::operator=(const PhasedArray<T, V>& Other)
{
    BeamDirection = Other.BeamDirection;
    Transmitters = Other.Transmitters;
    return *this;
}

template class ArcaWave::Radar::PhasedArray<float, 3>;
template class ArcaWave::Radar::PhasedArray<double, 3>;

