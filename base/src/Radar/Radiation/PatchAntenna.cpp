
#include "Radar/Radiation/PatchAntenna.h"

// STL includes
#include <cmath>
#include <iostream>

// ArcaWave incldues
#include "Utility/Constants.h"

using namespace std;
using namespace Eigen;
using namespace ArcaWave::Radar;
using namespace ArcaWave::Utility;

template<typename T, uint_fast32_t V>
PatchAntenna<T, V>::PatchAntenna(const Eigen::Matrix<std::complex<T>, 2, 1>& Polarization, const Matrix<T, V, 1>& Source, T Freq, T FreqSlope) :
    RadiationField<T ,V>(Polarization, Source, Freq, FreqSlope)
{}

template<typename T, uint_fast32_t V>
PatchAntenna<T, V>::~PatchAntenna()
{}

template<typename T, uint_fast32_t V>
T PatchAntenna<T, V>::Phase(T Time, const Matrix<T, V, 1>& Point) const
{
    // Computes the adjusted time to reach the point from the source and uses that to compute the phase
    const T adj_time = Time - ((Point - this->Source).norm() / C_0<T>);
    return -static_cast<T>(2) * PI<T> * adj_time * (this->Freq + (this->FreqSlope * adj_time / static_cast<T>(2)));
}

template<typename T, uint_fast32_t V>
T PatchAntenna<T, V>::Phase(T Time, T Dist) const
{
	const T adj_time = Time - (Dist / C_0<T>);
	return -static_cast<T>(2) * PI<T> * adj_time * (this->Freq + (this->FreqSlope * adj_time / static_cast<T>(2)));
}

template<typename T, uint_fast32_t V>
T PatchAntenna<T, V>::DopplerPhase(T Time, T Dist, T RelVel) const
{
    const T doppler = (C_0<T> - RelVel) / (C_0<T> + RelVel);
    return doppler * Phase(Time, Dist);
}

template<typename T, uint_fast32_t V>
T PatchAntenna<T, V>::Frequency(T Time, const Matrix<T, V, 1>& Point) const
{
    // Computes the adjusted time to reach the point from the source and uses that to compute frequency
    const T adj_time = Time - ((Point - this->Source).norm() / C_0<T>);
    return this->Freq + this->FreqSlope * adj_time;
}

template<typename T, uint_fast32_t V>
Matrix<complex<T>, V, 1> PatchAntenna<T, V>::ElectricField(T Time, const Matrix<T, V, 1>& Point) const
{    
    const complex<T> e = std::exp(I<T> * Phase(Time, Point));
    const Matrix<T, V, 1> propogation = Point - this->Source;
    const T r = propogation.norm();

    // If the distance between Point and Source is zero, then it is transmitter, project E field to electric direction (+x)
    if (r < FLOATING_POINT_ERROR<T>)
    {
        // const Matrix<T, V, 1> E_plane = Matrix<T, V, 1>(1, 0, 0);
        const Matrix<T, V, 1> E_plane = Matrix<T, V, 1>(0, 1, 0);
        return e * E_plane;
    }

    // Coordinate transfer (front, up, side): following calculation is based on (+z, +x, +y), expected is (+x, +y, +z)
    const Matrix<T, V, 1> new_prop = Matrix<T, V, 1>(propogation.y(), propogation.z(), propogation.x());

    // Convert cartesian to spherical coordinates for physics, and calculate unit vectors
    const T phi = atan2(new_prop.y(), new_prop.x());
    const T theta = atan2(sqrt((new_prop.x() * new_prop.x()) + (new_prop.y() * new_prop.y())), new_prop.z());
    const Matrix<T, V, 1> theta_unit(cos(theta) * cos(phi), cos(theta) * sin(phi), -sin(theta));
    const Matrix<T, V, 1> phi_unit(-sin(phi), cos(phi), 0);
    
    // The length and width of a single patch antenna. In fact L = length * k, W = width * k,
    // where k is free-space wavenumber, given by 2*pi/lambda.
    const T L = 0.5 * PI<T>;
    const T W = 0.8 * PI<T>;

    // Calculate the magnitude of E in direction of theta_unit and phi_unit
    const T divisor = W / 2 * sin(theta) * sin(phi);
    const T pattern_factor = divisor ? sin(divisor) / divisor : 0;
    const T array_factor = cos(L / 2 * sin(theta) * cos(phi));
    const T f_factor = pattern_factor * array_factor;
    const T E_theta = cos(phi) * f_factor;
    const T E_phi = -cos(theta) * sin(phi) * f_factor;

    // E_field should land on E plane, which is the xz plane
    Matrix<T, V, 1> E_field = (theta_unit * E_theta + phi_unit * E_phi) / r;
    return e * E_field;
}

template<typename T, uint_fast32_t V>
Matrix<complex<T>, V, 1> PatchAntenna<T, V>::MagneticField(T Time, const Matrix<T, V, 1>& Point) const
{
    // Take the cross product of the propogation vector and E field
    Matrix<T, V, 1> propogation = Point - this->Source;
    return propogation.cross(ElectricField(Time, Point)) / Z_0<T>;
}

template<typename T, uint_fast32_t V>
T PatchAntenna<T, V>::Gain(const Matrix<T, V, 1>& Propogation) const
{    
    // TODO: Need to convert coordinate in another way, still has some problem. Need to write in a
    // more clean way. Haven't added the input power gain.

    // Coordinate transfer (front, up, side): following calculation is based on (+z, +x, +y), expected is (+x, +y, +z)
    const Matrix<T, V, 1> new_prop = Matrix<T, V, 1>(Propogation.y(), Propogation.z(), Propogation.x());

    // Convert cartesian to spherical coordinates for physics, and calculate unit vectors
    const T phi = atan2(new_prop.y(), new_prop.x());
    const T theta = atan2(sqrt((new_prop.x() * new_prop.x()) + (new_prop.y() * new_prop.y())), new_prop.z());
    
    // The length and width of a single patch antenna. In fact L = length * k, W = width * k,
    // where k is free-space wavenumber, given by 2*pi/lambda.
    const T L = 0.5 * PI<T>;
    const T W = 0.8 * PI<T>;

    // Calculate the magnitude of E in direction of theta_unit and phi_unit
    const T divisor = W / 2 * sin(theta) * sin(phi);
    const T pattern_factor = divisor ? sin(divisor) / divisor : 0;
    const T array_factor = cos(L / 2 * sin(theta) * cos(phi));
    const T f_factor = pattern_factor * array_factor;
    const T E_theta = cos(phi) * f_factor;
    const T E_phi = -cos(theta) * sin(phi) * f_factor;

    T gain = sqrt(E_theta * E_theta + E_phi * E_phi);

    return gain;
}

// Explicitly defines templated instances
template class ArcaWave::Radar::PatchAntenna<float, 3>;
template class ArcaWave::Radar::PatchAntenna<double, 3>;