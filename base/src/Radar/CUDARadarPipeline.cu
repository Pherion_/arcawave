#include "Radar/CUDARadarPipelne.h"


namespace ArcaWave
{
	namespace Radar
	{
		namespace CUDA
		{
			template<typename T, uint_fast32_t V>
			__global__ void CalculateSamples(ArcaWave::Geometry::ProjectedRay<T, V>* HitInfo, ArcaWave::Radar::DeviceRadarPipelineInfo<T, V> RadInfo, Eigen::Matrix<thrust::complex<T>, V, 1>* Output)
			{
				uint32_t i = blockIdx.x;
				uint32_t f = threadIdx.x;
				const T sample_time = (f / RadInfo.SampleRate) + RadInfo.Time;
				T time = sample_time - ((RadInfo.Receivers[i] - HitInfo->Point).norm() / ArcaWave::Utility::C_0<T>);
				time -= ((RadInfo.Transmitters[0] - HitInfo->Point).norm() / ArcaWave::Utility::C_0<T>);
                if (time > 0)
				{
					T idxVal = time * 8e9;
					T interp = idxVal - (int(idxVal)*1.0);

					T realValA = RadInfo.TXSamples[int(idxVal)].real();
					T realValB = RadInfo.TXSamples[int(idxVal) + 1].real();
					T realVal = ((realValB - realValA) * interp) + realValA;

					T imagValA = RadInfo.TXSamples[int(idxVal)].imag();
					T imagValB = RadInfo.TXSamples[int(idxVal) + 1].imag();
					T imagVal = ((imagValB - imagValA) * interp) + imagValA;

//					T phase = RadInfo.RadField->Phase(time, HitInfo->Dist);
//                  T phase = fmod(RadInfo.RadField->Phase(time, HitInfo->Dist), 6.2831852);
                    // T phase = fmod(RadInfo.RadField->Phase(time, (HitInfo->Point - RadInfo.Transmitters[0]).norm()), 6.2831852);
                    // thrust::complex<T> wave = thrust::exp(thrust::complex<T>(0, 1) * phase);
					thrust::complex<T> wave(realVal, imagVal);
					auto rx = HitInfo->Amplitude * wave;

//					auto point = HitInfo->Point;
					// ArcaWave::Utility::CUDA::AtomicComplexVector<T, V>(&(Output[f * RadInfo.ReceiverCount + i]), rx);
					ArcaWave::Utility::CUDA::AtomicComplexVector<T, V>(&(Output[i * 1024 + f]), rx); // 1024 is sample count
				}
			}

			template <typename T, uint_fast32_t V>
			__global__ void ProcessHits(ArcaWave::Geometry::ProjectedRay<T, V>* Buffer, ArcaWave::Radar::DeviceRadarPipelineInfo<T, V> RadInfo, Eigen::Matrix<thrust::complex<T>, V, 1>* Output)
			{
				int block_id = blockIdx.x + blockIdx.y * gridDim.x;
				int thread_id = block_id * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
				if (Buffer[thread_id].RxVisible && Buffer[thread_id].Contact)
				{
                    CalculateSamples<T, V><<<RadInfo.ReceiverCount, RadInfo.SampleCount>>>(&(Buffer[thread_id]), RadInfo, Output);
				}
			}
		}
	}
}

using namespace ArcaWave::Radar;

template<typename T, uint_fast32_t V>
CUDARadarPipeline<T, V>::CUDARadarPipeline(const RadarPipelineInfo<T, V>& info) : RadarPipeline<T, V>(info)
{
	//@TODO move current_dev to a member?
	auto current_dev = cuda::device::get(0);

	auto transmitters = current_dev.memory().allocate(sizeof(Eigen::Matrix<T, V, 1>) * info.TransmitterCount);
	auto receivers = current_dev.memory().allocate(sizeof(Eigen::Matrix<T, V, 1>) * info.ReceiverCount);
	auto tx_samples = current_dev.memory().allocate(sizeof(thrust::complex<T>) * info.TXSampleCount);

	thrust::complex<T>* thrust_samples = new thrust::complex<T>[info.TXSampleCount];
	for(uint_fast32_t i = 0; i <  info.TXSampleCount; ++i)
	{
		std::complex<T> c = info.TXSamples->at(i);
		thrust_samples[i] = thrust::complex<T>(c.real(), c.imag());
	}

	cuda::memory::copy(transmitters, info.Transmitters.get(), sizeof(Eigen::Matrix<T, V, 1>) * info.TransmitterCount);
	cuda::memory::copy(receivers, info.Receivers.get(), sizeof(Eigen::Matrix<T, V, 1>) * info.ReceiverCount);
	cuda::memory::copy(tx_samples, thrust_samples, sizeof(thrust::complex<T>) * info.TXSampleCount);
	delete thrust_samples;

	DeviceInfo = DeviceRadarPipelineInfo<T, V>
		{
			info.Time,
			info.SampleRate,
			info.SampleCount,
			info.TransmitterCount,
			info.ReceiverCount,
			info.TXSampleCount,
			reinterpret_cast<Eigen::Matrix<T, V, 1>*>(transmitters),
			reinterpret_cast<Eigen::Matrix<T, V, 1>*>(receivers),
			reinterpret_cast<thrust::complex<T>*>(tx_samples),
			ArcaWave::Utility::ConstructOnDevice<PlaneWave<T, V>,
			Matrix<T, V, 1>, Matrix<std::complex<T>, 2, 1>, Matrix<T, V, 1>, T, T>
			(current_dev, Eigen::Matrix<T, V, 1>{1, 0, 0}, info.RadField->Polarization, info.RadField->Source, info.RadField->Freq, info.RadField->FreqSlope)
		};
}

template<typename T, uint_fast32_t V>
CUDARadarPipeline<T, V>::~CUDARadarPipeline()
{
    cuda::memory::device::free(DeviceInfo.Transmitters);
    cuda::memory::device::free(DeviceInfo.Receivers);
	cuda::memory::device::free(DeviceInfo.TXSamples);
    ArcaWave::Utility::DestructOnDevice<RadiationField<T, V>>(cuda::device::get(0), DeviceInfo.RadField);
}

template<typename T, uint_fast32_t V>
void CUDARadarPipeline<T, V>::ComputeRXSignalFarField(const std::shared_ptr<RadarFrame<T, V>>& Frame)
{
	// Project rays using the Ray Tracer
	// @TODO make this input parameter
	auto launch = LaunchInfo<T, V>();
	launch.Height  = 1024;
	launch.Width = 1024;
//	launch.Pos = RadarPipeline<T, V>::Info.Transmitters[0];
    launch.Pos = Matrix<T, V, 1>(-6.0, 1.5, 0.0);
	launch.Ori = RadarPipeline<T, V>::Info.TransOrientations[0];

	// Goes through each ray created in the window
	auto rays = std::dynamic_pointer_cast<OptixRayTracer<T, V>>(RadarPipeline<T, V>::Info.RTX)->ProjectRaysDevice(launch);

    auto begin = std::chrono::system_clock::now();
    // Allocate memory for output on host
	auto num_elements = RadarPipeline<T, V>::Info.ReceiverCount * RadarPipeline<T, V>::Info.SampleCount;
	Frame->RXSignal = std::shared_ptr<Matrix<std::complex<T>, V, 1>[]>(new Matrix<std::complex<T>, V, 1>[num_elements]);

	// Allocate memory for output on device
	auto current_dev = cuda::device::get(0);
	auto output_mem = reinterpret_cast<Eigen::Matrix<thrust::complex<T>, V, 1>*>(current_dev.memory().allocate(sizeof(Eigen::Matrix<thrust::complex<T>, V, 1>) * num_elements));
	cuda::memory::zero(output_mem, sizeof(Eigen::Matrix<thrust::complex<T>, V, 1>) * num_elements);
	current_dev.synchronize();

	//compute launch size
	auto launch_size = cuda::make_launch_config(cuda::grid::dimensions_t(launch.Height *2, launch.Width * 2), cuda::grid::dimensions_t(1,1));
	current_dev.launch(ArcaWave::Radar::CUDA::ProcessHits<T, V>,
	    launch_size,
		reinterpret_cast<ProjectedRay<T, V>*>(rays), DeviceInfo, output_mem
	    );

	current_dev.synchronize();
	// copy result back to host
	cuda::memory::copy(Frame->RXSignal.get(), output_mem, sizeof(Eigen::Matrix<thrust::complex<T>, V, 1>) * num_elements);

	// free memory on device
	cuda::memory::device::free(output_mem);
	cuda::memory::device::free(rays);

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double> t = end - begin;
    std::cout << "CUDA Reduction finished in: " << t.count() << std::endl;
}

// Explicitly defines templated instances
template class ArcaWave::Radar::CUDARadarPipeline<float, 3>;
template class ArcaWave::Radar::CUDARadarPipeline<double, 3>;