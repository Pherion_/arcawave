#include "Radar/ObjectSlicer.h"
#include "Geometry/Mesh.h"

#include <iostream>
#include <memory>

template<typename T, uint_fast32_t V>
ArcaWave::Radar::ObjectSlicer<T, V>::ObjectSlicer() = default;

template<typename T, uint_fast32_t V>
ArcaWave::Radar::ObjectSlicer<T, V>::~ObjectSlicer() = default;

template<typename T, uint_fast32_t V>
void ArcaWave::Radar::ObjectSlicer<T, V>::Slice(std::shared_ptr<Object<T, V>> Obj, std::vector<AABB<T, V>> BBoxes)
{

	std::vector<std::shared_ptr<std::shared_ptr<Submesh>[]>> submesh_vector;
	for (uint_fast32_t i = 0; i < BBoxes.size(); ++i)
		submesh_vector.push_back(std::shared_ptr<std::shared_ptr<Submesh>[]>(new std::shared_ptr<Submesh>[Obj->GetMeshCount()]));

	for (uint_fast32_t i = 0; i < Obj->GetMeshCount(); ++i)
	{
		auto mesh = Obj->GetMesh(i);
		for (uint_fast32_t j = 0; j < Obj->GetMesh(i)->GetFaceCount(); ++j)
		{
			auto mesh_face = Obj->GetMesh(i)->GetFace(j);
			for (uint_fast32_t k = 0; k < BBoxes.size(); ++k)
			{
				uint_fast32_t num_points_in_bbox =
					BBoxes[k].NumPointsInAABB(std::vector<Eigen::Matrix<T, V, 1>>{ mesh->GetVertex(mesh_face.A),
																				   mesh->GetVertex(mesh_face.B),
																				   mesh->GetVertex(mesh_face.C) });
				if (num_points_in_bbox == 3)
				{
					if (submesh_vector[k][i] == nullptr)
						submesh_vector[k][i] = std::make_shared<Submesh>();

					submesh_vector[k][i]->AddFace(j);
				}
			}
		}
	}


}

template<typename T, uint_fast32_t V>
std::vector<AABB<T, V>> ArcaWave::Radar::ObjectSlicer<T, V>::PartitionAABB(const AABB<T, V>& BBox, Eigen::Vector3i PartitionCount)
{
	std::vector<AABB<T, V>> boxes;

	Eigen::Matrix<T, V, 1> length =
		{
			(BBox.Max.x() - BBox.Min.x()) / PartitionCount.x(),
			(BBox.Max.y() - BBox.Min.y()) / PartitionCount.y(),
			(BBox.Max.z() - BBox.Min.z()) / PartitionCount.z()
		};

	for (uint_fast32_t i = 0; i < PartitionCount.x(); i++)
	{
		for (uint_fast32_t j = 0; j < PartitionCount.y(); j++)
		{
			for (uint_fast32_t k = 0; k < PartitionCount.z(); k++)
			{
				Eigen::Matrix<T, V, 1> min =
					{
						BBox.Min.x() + (i * length.x()),
						BBox.Min.y() + (j * length.y()),
						BBox.Min.z() + (k * length.z())
					};

				Eigen::Matrix<T, V, 1> max =
					{
						min.x() + length.x(),
						min.y() + length.y(),
						min.z() + length.z(),
					};
				boxes.push_back(AABB<T, V>(max, min));
			}
		}
	}
	return boxes;
}



template class ArcaWave::Radar::ObjectSlicer<float, 3>;
template class ArcaWave::Radar::ObjectSlicer<double, 3>;
