
#include "Radar/BakingPipeline.h"

// template<typename T, uint_fast32_t V>
// std::shared_ptr<RadarObject<T, V>> BakePipeline<T, V>::Bake(const std::filesystem::path& ObjectPath)
// {
// 	// import the object
// 	auto rad_obj = std::make_shared<RadarObject<T, V>>(ObjectPath);

// 	// // Slice it @ TODO
// 	// auto sub_bboxes = Info.ObjectSlicerUnit->PartitionAABB(rad_obj->GetBoundingBox(), Info.SliceParams);
// 	// //auto sub_objs = Info.ObjectSlicerUnit->Slice(rad_obj, sub_bboxes);

// 	// //build the vcm
// 	// auto vcm_bake_info = VCMBakeInfo<T, V>
// 	// 	{
// 	// 		Info.Frequency,
// 	// 		Info.Polarization,
// 	// 		Info.IncThetaInterval,
// 	// 		Info.IncPhiInterval,
// 	// 		Info.RefThetaInterval,
// 	// 		Info.IncPhiInterval,
// 	// 		rad_obj,
// 	// 		Info.RayTracerUnit,
// 	// 		Info.DiscretizerUnit,
// 	// 		Info.ThreadPoolUnit,
// 	//		Info.CurrentCalculatorUnit
// 	// 	};
// 	// auto vcm = VCM<T, V>::Bake(vcm_bake_info);
// 	// rad_obj->AddVCM(vcm, rad_obj->GetBoundingBox());

// 	return rad_obj;
// }

// template<typename T, uint_fast32_t V>
// BakePipeline<T, V>::BakePipeline(BakePipelineInfo<T, V> Info) : Info(Info)
// {}


// // Explicitly defines templated instances
// template class ArcaWave::Radar::BakePipeline<float, 3>;
// template class ArcaWave::Radar::BakePipeline<double, 3>;