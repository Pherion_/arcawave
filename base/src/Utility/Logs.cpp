
#include <iostream>
#include "Utility/Logs.h"


using namespace std;
using namespace spdlog;
using namespace spdlog::sinks;
using namespace ArcaWave::Utility;


Logs::Logs()
{}


Logs::Logs(int thrd_num)
{
    try{
        //creating console logger/file
        auto file_logger = make_shared<spdlog::sinks::basic_file_sink_mt>("logs/console_log.txt", true);
        auto stdout_logger = make_shared<spdlog::sinks::stdout_color_sink_mt>();
        auto console_logger = make_shared<spdlog::logger>("console", spdlog::sinks_init_list {file_logger, stdout_logger});
        spdlog::register_logger(console_logger);
        console_logger->sinks()[1]->set_level(spdlog::level::warn);
        console_logger->set_pattern("%^%v%$");
        console_logger->info("┌{0:-^{2}}┐\n|{1: ^{2}}|\n└{0:-^{2}}┘\n ", "", "Console Log", 80);
        console_logger->set_pattern("[%H:%M:%S]%^[console]%$ %v");
        console_logger->sinks()[1]->set_level(spdlog::level::info);
        
        //Creating thread output file
        init_thread_pool(thrd_num, 4);
        auto asyncLogger = create_async<basic_file_sink_mt>("async_file_logger","logs/async_file.txt");
        asyncLogger->set_pattern("[%H:%M:%S][T %t] %v");
        
        //Creating error exception log
        auto error_sink = make_shared<basic_file_sink_mt>("logs/error_log.txt", true);
        error_sink->set_level(spdlog::level::info);
        auto std_error = make_shared<stdout_color_sink_mt>();
        std_error->set_level(spdlog::level::warn);
        std_error->set_pattern("%^*** Err ***%$ %v");
        auto error_logger = make_shared<logger>("error_logger", spdlog::sinks_init_list{error_sink, std_error});
        error_logger->sinks()[0]->set_pattern("%v");
        error_logger->info("{0:*^{2}}\n{1: ^{2}}\n{0:*^{2}}\n ", "", "Error Log", 80);
        error_logger->sinks()[0]->set_pattern("[%H:%M:%S][error] %v");
        register_logger(error_logger);
       
    }
    catch (spdlog_ex & ex)
    {
        cout << "Log initialization error: " << ex.what() << endl;
        exit(1);
    }

}


void Logs::updateStatus(float cur)
{
    float progress = cur > 1.0 ? 1.0:cur;
    int barWidth = 100;
    int pos = barWidth * progress;
    cout << "[";
    for (int i = 0; i <= barWidth; ++i) {
        if (i < pos) cout << "=";
        else if (i == pos) cout << ">";
        else cout << " ";
    }
    cout << "] " << int(progress * 100.0) << " %\r";
    cout.flush();
    this_thread::sleep_for(chrono::seconds(1)); 
    // progress += 0.1; // for demonstration only
    if(progress == 1.0) cout << endl;
}

// void ArcaWave::Utility::Error(string msg, const std::experimental::source_location &location)
// {
//     get("error_logger")->error("@ {0}:{1}\nMessage: {2}", location.function_name(), location.line(), msg);
//     exit(1);
// }

void ArcaWave::Utility::Error(std::exception &e, const std::experimental::source_location &location)
{
    get("error_logger")->error("@ {0}:{1}\nException Message: {2}", location.function_name(), location.line(), e.what());
    exit(1);
}


void ArcaWave::Utility::check(bool equal, string msg, int level, const std::experimental::source_location &location)
{
    if(!equal) {
        try{
            switch(level){
                case 0: {
                    get("error_logger")->error("{0}:{1} ==>{2} assertion Failed", location.file_name(), location.line(), msg);  //err
                    exit(1);
                }
                case 1: get("error_logger")->warn("{0}:{1} ==>{2} assertion Failed", location.file_name(), location.line(), msg); //warn
            }
        }catch(spdlog_ex & ex){
            cout << "Logging error: " << ex.what() << endl;
        }
    }
    
}

void ArcaWave::Utility::check(nlohmann::json input)
{

    int tx_height, tx_width, rx_height, rx_width;
    tx_width = input["radar_params"]["tx_width"];
    tx_height = input["radar_params"]["tx_height"];
    // rx_width = input["radar_params"]["rx_width"];
    // rx_height = input["radar_params"]["rx_height"];
    try{
        check(input["radar_params"]["tx_width"] > 0, "Tx width");
        check(input["radar_params"]["tx_height"] > 0,"Tx height");
        check(input["radar_params"]["rx_width"] > 0, "Rx width");
        check(input["radar_params"]["rx_height"] > 0,"Rx height");
    }catch(exception &e){
        Error(e);
    }

    try{
        check(input["radar_params"]["orientation"].size() == 9, "radar orientation number == 9");
        check(input["radar_params"]["tx_position"].size() == 3 * tx_height * tx_width, "tx give positions matches tx number");
        // check(input["radar_params"]["rx_position"].size() == int(3 * rx_height * rx_width), "rx give positions matches rx number");
    }catch(exception &e){
        Error(e);
    }

    try{
        std::filesystem::path p = input["objects"][0];
        string pathjson = p.string() + ".json";
        string pathmtl = p.string() + ".mtl";
        string pathobj = p.string() + ".obj";
        check(std::filesystem::exists(pathjson), "json filepath valid");
        check(std::filesystem::exists(pathmtl), "mtl filepath valid", 1);
        check(std::filesystem::exists(pathobj), "obj filepath valid", 1);

    }catch (exception &e){
        Error(e);
    }

    try{
        for (const auto& instance : input["instances"])
	    {
	    	uint_fast32_t object_index = instance["object_index"];

            check(object_index >= 0, "object index valid");   
	    	check(instance["position"].size() == 3, "object position has 3 values");
            check(instance["orientation"].size() == 9, "object orientation has 9 values");
	    }
    }catch (exception &e){
        Error(e);
    }

}
