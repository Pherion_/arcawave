
#include "Utility/ThreadPool.h"

using namespace std;
using namespace ArcaWave::Utility;

ThreadPool::ThreadPool(uint_fast32_t MaxThreadCount, uint_fast32_t MaxJobCount) :
    MaxThreadCount(MaxThreadCount), 
    MaxJobCount(MaxJobCount),
    JobMutex(),
    CurrentJobs(0),
    WorkerMutexes(new mutex[MaxThreadCount]),
    WorkerThreads(new shared_ptr<thread>[MaxThreadCount])
{
    for(uint_fast32_t i = 0; i < MaxThreadCount; ++i)
        WorkerThreads[i].reset();
}

ThreadPool::~ThreadPool()
{
    // Locks the job mutex to prevent other jobs being dispatched, and waits for each thread to finish execution
    JobMutex.lock();
    for(uint_fast32_t i = 0; i < MaxThreadCount; ++i)
        WorkerMutexes[i].lock();
}

uint_fast32_t ThreadPool::GetMaxThreads() const
{
    return MaxThreadCount;
}

uint_fast32_t ThreadPool::GetMaxJobs() const
{
    return MaxJobCount;
}

bool ThreadPool::Dispatch(uint_fast32_t RequestedThreads, JobFunction Function, const shared_ptr<JobParameter>& Parameter)
{
    // Attempts to check if there is room for the requested job, and if not, return failure, otherwise increment the number of current jobs
    unique_lock<mutex> job_lock(JobMutex);
    if(CurrentJobs >= MaxJobCount)
        return false;
    ++CurrentJobs;
    job_lock.unlock();

    // Dispatches the job and waits until it is completed
    thread job(ThreadPool::RunJob, this, RequestedThreads, Function, Parameter);
    job.join();

    // Once the job is finished, decrement the number of current jobs, and return true.
    job_lock.lock();
    --CurrentJobs;
    return true;
}

void ThreadPool::RunJob(ThreadPool* Pool, uint_fast32_t RequestedThreads, JobFunction Function, const shared_ptr<JobParameter>& Parameter)
{
    // Try to acquire the requested number of threads
    shared_ptr<uint_fast32_t[]> thread_ids(new uint_fast32_t[RequestedThreads]);
    uint_fast32_t reserved = 0;
    while(reserved < RequestedThreads)
    {
        for(uint_fast32_t i = 0; i < Pool->MaxThreadCount; ++i)
        {
            if(Pool->WorkerMutexes[i].try_lock())
            {
                thread_ids[reserved] = i;
                ++reserved;
                if(reserved >= RequestedThreads)
                    break;
            }
        }
    }

    // Creates all the threads
    for(uint_fast32_t i = 0; i < RequestedThreads; ++i)
    {
        shared_ptr<thread> thr = make_shared<thread>(Function, i, RequestedThreads, Parameter);
        Pool->WorkerThreads[thread_ids[i]] = thr;
    }

    // Waits for each thread to finish its execution and unlocks each mutex
    for(uint_fast32_t i = 0; i < RequestedThreads; ++i)
    {
        Pool->WorkerThreads[thread_ids[i]]->join();
        Pool->WorkerMutexes[i].unlock();
    }
}
