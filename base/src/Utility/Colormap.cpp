
#include "Utility/Colormap.h"

// STL includes
#include <fstream>

using namespace std;
using namespace ArcaWave::Utility;

template<typename T>
Colormap<T>::Colormap(const vector<Color<T>>& Colors) :
    Colors(Colors)
{}

template<typename T>
Colormap<T>::~Colormap()
{}

template<typename T>
Color<T> Colormap<T>::Map(T Value)
{
    uint32_t index;
    if(Value >= 1.0)
        index = Colors.size() - 1;
    else if(Value < 0.0)
        index = 0;
    else
        index = (uint32_t)(Value * Colors.size());
    return Colors[index];
}

template<typename T>
Colormap<T> Colormap<T>::Import(const string& Filename)
{
    ifstream f(Filename, ifstream::in);
    uint32_t size = 0;
    vector<Color<T>> colors;
    float R = 0.0; 
    float G = 0.0; 
    float B = 0.0;
    float A = 0.0;
    f >> size;
    for(int i = 0; i < size; ++i)
    {
        f >> R >> G >> B >> A;
        colors.push_back({R, G, B, A});
    }
    f.close();

    return Colormap(colors);
}

// Explicitly defines templated instances
template class ArcaWave::Utility::Colormap<float>;
template class ArcaWave::Utility::Colormap<double>;
