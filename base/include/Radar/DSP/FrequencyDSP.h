#pragma once

// ArcaWave includes
#include "Radar/DSP/SignalProcessing.h"

namespace ArcaWave::Radar
{
    template<typename T, uint_fast32_t V>
    class FrequencyDSP : public SignalProcessing<T, V>
    {
    // Public constructors and deconstructor
    public:
        FrequencyDSP(const SignalProcessingInfo<T, V>& Info, const std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> Data);

        virtual ~FrequencyDSP();

    // Public methods
    public:
        virtual void RangeEstimation();

        virtual void AngleEstimation();

        virtual void VelocityEstimation();
    };
}
