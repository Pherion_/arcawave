#pragma once

#ifdef __clang__
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Weverything"
#elif __GNUG__
	#pragma GCC diagnostic push
	#pragma GCC diagnostic ignored "-Wall"
#endif
#include <unsupported/Eigen/FFT>
#ifdef __clang__
	#pragma clang diagnostic pop
#elif __GNUG__
	#pragma GCC diagnostic pop
#endif

// #include <stdlib.h>
#include <memory>
// #include <complex>
// #include "Radar/RadarPipeline.h"
// #include "Radar/RadarClass.h"


namespace ArcaWave::Radar
{

	/* A input informatin struct for DSP use */
	template<typename T, uint_fast32_t>
    struct DSPprocess
    {
        uint_fast32_t r_rows;
        uint_fast32_t r_cols;
        uint_fast32_t num_samples;
		Eigen::Matrix<std::complex<T>, 2, 1> polarization;
    };


	template<typename T, uint_fast32_t V>
	class FFTEngine
	{
		public:

			FFTEngine(){}

			FFTEngine(
				std::vector<Eigen::Matrix<std::complex<T>, V, 1>>& Mixed, 
				DSPprocess<T,V>& info)
			{}
		
			virtual ~FFTEngine()
			{}

		protected:

            std::shared_ptr<std::vector<std::vector<std::vector<std::complex<T>>>>> RawData;

            std::shared_ptr<std::vector<Eigen::Matrix<std::complex<T>, V, 1>>> Range;

			virtual void processing(DSPprocess<T,V>& info) = 0;


		public:

            virtual std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::complex<T>>>>>>> forward_fft() = 0;

			virtual std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::shared_ptr<std::vector<std::complex<T>>>>>>> fft_shift() = 0;
		
	};
}