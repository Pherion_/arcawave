#pragma once

//STL Includes
#include <random>
#include <shared_mutex>

// ArcaWave includes
#include "Radar/Noise/NoiseSource.h"

namespace ArcaWave::Radar
{
	template<typename T, uint_fast32_t V>
	class BandLimitedNoiseSource : public NoiseSource<T, V>
	{
		// Private fields
	 private:

		/**
		 * @brief Used as a read/write lock for thread safe mutation
		 */
		mutable std::shared_mutex GranularityLock;

		/**
		 * @brief No. of frequency tones present in noise at any point
		 */
		uint_fast32_t Granularity = 0;

		/**
		 * @brief The set of frequency tones currently being used; drawn from a uniform distribution
		 */
		std::vector<T> CurrentFrequency;

		/**
		 * @brief The current amplitude of noise; drawn from normal distribution.
		 */
		T CurrentAmplitude;

		/**
		 * @brief Nyquist rate  required for sampling the bandlimited noise
		 */
		T SamplingRate;

		/**
		 * @brief Frequencies are drawn from this
		 */
		std::uniform_real_distribution<T> UniformDistribution;

		/**
		 * @brief Amplitude is drawn from this
		 */
		std::normal_distribution<T> GaussianDistribution;

		// Public constructor and destructor
	 public:
		/**
		 * @brief Create a source of bandlimited Noise
		 * @param seed Seed for RNG engine
		 * @param averagePower Average RMS power of the generated noise
		 * @param minFreq Lower bound for tones in generated noise
		 * @param maxFreq Upper bound for tones in generated noise
		 * @param granularity Number of frequencies the generated noise should be composed of
		 */
		BandLimitedNoiseSource(uint_fast32_t seed, T averagePower, T minFreq, T maxFreq, uint_fast32_t granularity);

		/**
		 * @brief Default destructor
		 */
		~BandLimitedNoiseSource();

		// Public methods
	 public:
		/**
		 * @brief Interface for accessing the generated noise signal
		 * @param index Required to generated bandlimited noise; sequence must correspond to the
		 * sequence of the signal noise is being added to
		 * @return Generated noise signal vector
		 */
		virtual Eigen::Matrix<T, V, 1> Noise(uint_fast32_t index = 0) override;

		/**
		 * @brief Change the average power of generated noise.
		 * @param averagePower
		 */
		void SetAveragePower(T averagePower) override;

		/**
		 * @brief Change the bandwidth of generated noise
		 * @param minFreq lower bound for tones in noise
		 * @param maxFreq upper bound for tones in noise
		 */
		void SetBandwidth(T minFreq, T maxFreq);
	};
}
