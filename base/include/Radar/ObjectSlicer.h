#pragma once

// STL includes
#include <memory>

// Arcwave includes
#include "Geometry/Object.h"

using namespace ArcaWave::Geometry;


namespace ArcaWave::Radar
{
	/**
	 * @brief
	 */
	template<typename T, uint_fast32_t V>
	class ObjectSlicer
	{

	 private:

		// Public Constructors and destructors
	 public:

		/**
		 * @brief
		 */
		ObjectSlicer();

		/**
		 * @brief
		 */
		std::vector<AABB<T, V>> PartitionAABB(const AABB<T, V>& BBox, Eigen::Vector3i PartitionCount);

		/**
		 * @brief
		 */
		~ObjectSlicer();

		/**
		 * @brief
		 */
		void Slice(std::shared_ptr<Object<T, V>> Obj, std::vector<AABB<T, V>> BBoxes);


	};

}
