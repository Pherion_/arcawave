
#pragma once

// ArcaWave includes
#include "Geometry/Object.h"
#include "Geometry/AABB.h"
#include "Geometry/Partition.h"
#include "Radar/VCM.h"

// STL Includes
#include <filesystem>
#include <memory>
#include <utility>
#include <vector>

namespace ArcaWave::Radar
{
	template<typename T, uint_fast32_t V>
	struct RadarObjectCreateInfo
	{
	
	// Public fields
	public:

		std::shared_ptr<Object<T, V>> Obj;
		bool XSym;
		bool YSym;
		bool ZSym;
		uint_fast32_t PartitionCount;
		std::shared_ptr<uint_fast32_t[]> MeshIndicies;
		std::shared_ptr<std::shared_ptr<Partition<T, V>>[]> Partitions;
	};

	template<typename T, uint_fast32_t V>
	class RadarObject : public Object<T, V>
	{

	// Private fields
	public:

		std::shared_ptr<std::shared_ptr<Partition<T, V>>[]> Partitions;
		uint_fast32_t VCMCount;
		std::shared_ptr<std::shared_ptr<VCM<T, V>>[]> VCMSet;


	// Public constructors and deconstructor
	public:

		RadarObject(const std::shared_ptr<Object<T, V>>& Obj);

		RadarObject(const std::shared_ptr<RadarObjectCreateInfo<T, V>>& Info);

		virtual ~RadarObject();

	// Public methods
	public:

		std::shared_ptr<Partition<T, V>> GetPartition(uint_fast32_t Index);

		void Bake(const std::shared_ptr<VCMBuildInfo<T, V>>& BakeInfo, uint_fast32_t ThreadCount);

	// Public static methods
	public:

	 	static void Export(const std::filesystem::path& Path, const std::shared_ptr<RadarObject<T, V>>& RadObj);

		static std::shared_ptr<RadarObject<T, V>> Import(const std::filesystem::path& Path);

	// Private static methods
	private:

		static std::vector<std::pair<std::shared_ptr<Partition<T, V>>, uint_fast32_t>> ExpandPartitions(const std::shared_ptr<RadarObjectCreateInfo<T, V>>& Info);

		static std::shared_ptr<ObjectCreateInfo<T, V>> MakePartitions(const std::shared_ptr<RadarObjectCreateInfo<T, V>>& Info);

	};
}