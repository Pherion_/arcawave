#pragma once

// STL includes
#include <memory>
#include <vector>

// ArcaWave includes
#include "Geometry/Environment.h"
#include "Geometry/RayTracing/RayTracer.h"
#include "Radar/Discretizer.h"
#include "Radar/Radiation/RadiationField.h"
#include "Utility/ThreadPool.h"
#include "Radar/VCM.h"

namespace ArcaWave::Radar
{
    template<typename T, uint_fast32_t V>
    struct RadarPipelineInfo
    {
        std::shared_ptr<Utility::ThreadPool> ThrdPool;
        std::shared_ptr<Geometry::Environment<T, V>> Env;
        std::shared_ptr<Geometry::RayTracer<T, V>> RTX;
        std::shared_ptr<Discretizer<T, V>> Disc;

        // TODO: Temporary
        std::shared_ptr<VCM<T, V>> VectorCurrentMoment;
        std::shared_ptr<RadiationField<T, V>> RadField;
        uint_fast32_t SampleCount;
        T SampleRate;
        uint_fast32_t TransmitterCount;
        uint_fast32_t ReceiverCount;
        std::shared_ptr<Eigen::Matrix<T, V, 1>[]> Transmitters;
        std::shared_ptr<Eigen::Matrix<T, V, V>[]> TransOrientations;
        std::shared_ptr<Eigen::Matrix<T, V, 1>[]> Receivers;
        std::shared_ptr<std::vector<std::complex<T>>> TXSamples;
        uint_fast32_t TXSampleCount;
        T Time;
    };

    template<typename T, uint_fast32_t V>
    struct RadarFrame
    {
        T Time;
        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> TXSignal;
        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> RXSignal;
        std::shared_ptr<Eigen::Matrix<std::complex<T>, V, 1>[]> MXSignal;
    };

    template<typename T, uint_fast32_t V>
    class RadarPipeline
    {

    // Private sub-structures
	 protected:

        struct NearFieldJobParameter : public Utility::JobParameter
        {
            RadarPipelineInfo<T, V> Info;
            std::shared_ptr<RadarFrame<T, V>> Frame;
            Eigen::Matrix<T, V, 1> Position;
        };

    // Private fields
	 protected:

        RadarPipelineInfo<T, V> Info;

    // Public constructors and deconstructor
    public:

        RadarPipeline(const RadarPipelineInfo<T, V>& Info);

        virtual ~RadarPipeline();

    // Public methods
    public:

        virtual void RunFrameFarField(const std::shared_ptr<RadarFrame<T, V>>& Frame);

        virtual void RunFrameNearField(const std::shared_ptr<RadarFrame<T, V>>& Frame);

    // Private methods
	 protected:

        virtual void ComputeTXSignal(const std::shared_ptr<RadarFrame<T, V>>& Frame);

        virtual void ComputeRXSignalNearField(const std::shared_ptr<RadarFrame<T, V>>& Frame);

        virtual void ComputeRXSignalFarField(const std::shared_ptr<RadarFrame<T, V>>& Frame);

        virtual void ComputeMXSignal(const std::shared_ptr<RadarFrame<T, V>>& Frame);

    // Private static methods
	 protected:

        static void NearFieldJobFunction(uint_fast32_t ThreadID, uint_fast32_t ThreadCount, const std::shared_ptr<Utility::JobParameter>& Parameter);
    };
}
