#pragma once

// STL includes
#include <memory>
#include <vector>

// ArcaWave includes
#include "VCM.h"
#include "ObjectSlicer.h"
#include "RadarObject.h"
#include "Calculator/CurrentCalculator.h"

// namespace ArcaWave::Radar
// {
// 	template<typename T, uint_fast32_t V>
// 	struct BakePipelineInfo
// 	{
// 		//Processing units
// 		std::shared_ptr<Utility::ThreadPool> ThreadPoolUnit;
// 		std::shared_ptr<Geometry::RayTracer<T, V>> RayTracerUnit;
// 		std::shared_ptr<Discretizer<T, V>> DiscretizerUnit;
// 		std::shared_ptr<ObjectSlicer<T, V>> ObjectSlicerUnit;
// 		std::shared_ptr<CurrentCalculator<T, V>> CurrentCalculatorUnit;

// 		//Parameters
// 		T Frequency;
// 		Eigen::Matrix<std::complex<T>, 2, 1> Polarization;
// 		Utility::Interval<T> IncThetaInterval;
// 		Utility::Interval<T> IncPhiInterval;
// 		Utility::Interval<T> RefThetaInterval;
// 		Utility::Interval<T> RefPhiInterval;
// 		Eigen::Vector3i SliceParams;
// 	};

// 	template<typename T, uint_fast32_t V>
// 	class BakePipeline
// 	{
// 	 private:
// 		BakePipelineInfo<T, V> Info;

// 	 public:
// 		BakePipeline(BakePipelineInfo<T, V> Info);

// 		std::shared_ptr<RadarObject<T, V>> Bake(const std::filesystem::path& ObjectPath);
// 	};
// }
