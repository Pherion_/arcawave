
#pragma once

// ArcaWave includes
#include "Radar/Radiation/RadiationField.h"

namespace ArcaWave::Radar
{
    /**
     * @brief Represents an electromagnetic radiation field that propogates from some source with a planar wave front. This can also be used to represent a frequency-moduleated,
     * continuous wave (FMCW) plane wave by giving it a non-zero frequency slope.
     * 
     * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
     * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
     */
    template<typename T, uint_fast32_t V>
    class PlaneWave : public RadiationField<T, V>
    {

    // Protected fields
    public:

        /**
         * @brief The vector of propogation describing the direction of travel for the plane wave.
         */
        Eigen::Matrix<T, V, 1> Propogation;

        /**
         * @brief The vector parallel to the horizontal plane normal to the propogation vector of the plane wave.
         */
        // Eigen::Matrix<T, V, 1> HorizontalPlane;

        /**
         * @brief The second vector parallel to the plane normal to the propogation vector of the plane wave.
         * @brief The vector parallel to the vertical plane normal to the propogation vector of the plane wave.
         */
        // Eigen::Matrix<T, V, 1> VerticalPlane;

        /**
         * @brief The first vector parallel to the plane normal to the propogation vector of the plane wave.
         */
        Eigen::Matrix<T, V, 1> PlaneA;

        /**
         * @brief The second vector parallel to the plane normal to the propogation vector of the plane wave.
         */
        Eigen::Matrix<T, V, 1> PlaneB;

    // Public constructors and deconstructor
    public:

        /**
         * @brief Default constructor of PlaneWave.
         */
        PlaneWave();

        /**
         * @brief Creates the plane wave from the propogation vector, polarization, source, frequency, and frequency slope. This can also be used to represent a
         * frequency modulated, continuous wave (FMCW) plane wave if a non-zero frequency slope is given.
         * 
         * @param Propogation - The direction of propogation for the plane wave.
         * @param Polarization - The polarization of the plane wave.
         * @param Source - The source of the plane wave, which represents the initial plane orthogonal to propogation vector which is the given frequency at time zero.
         * @param Freq - The frequency at the source of the plane wave at time zero. Is the constant frequency of the entire plane wave if frequency slope is zero.
         * @param FreqSlope - The frequency slope of the plane wave, which defaults to a value of zero.
         */
        CUDA_HOSTDEV PlaneWave
        (
            const Eigen::Matrix<T, V, 1> Propogation,
            const Eigen::Matrix<std::complex<T>, 2, 1>& Polarization, 
            const Eigen::Matrix<T, V, 1>& Source, 
            T Freq, 
            T FreqSlope = static_cast<T>(0)
        );
        /**
         * @brief Copy constructor for the plane wave, which copies the values from the given plane wave into the newly created plane wave.
         * 
         * @param Other - The other plane wave to copy the values from.
         */
        PlaneWave(const PlaneWave<T, V>& Other);

        /**
         * @brief Destroys the plane wave.
         */
        CUDA_HOSTDEV virtual ~PlaneWave();

    // Public operators
    public:

        /**
         * @brief The assignment operator which copies the values from a given plane wave into this plane wave.
         * 
         * @param Other - The other plane wave to copy the values from.
         * @return PlaneWave& - A reference to this plane wave which was copies into.
         */
        PlaneWave& operator=(const PlaneWave& Other);
        // Public methods
    public:

        /**
         * @brief Gets the phase difference between the given point and the source of the plane wave at the given time.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Point - The point in the world frame at which to compute the phase difference from the source of the plane wave.
         * @return T - The phase difference between the source and the given point at the given time.
         */
        virtual T Phase(T Time, const Eigen::Matrix<T, V, 1>& Point) const;
        
        /**
         * @brief Gets the phase difference given distance.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Dist - The distance between source and the point in the world frame.
         * @return T - The phase difference between the source and the given point.
         */
		CUDA_HOSTDEV virtual T Phase(T Time, T Dist) const;

        /**
         * @brief Gets the phase difference given distance, account for doppler shift.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Dist - The distance between source and the point in the world frame.
         * @param RelVel - The relative velocity between source and given point.
         * @return T - The phase difference between the source and the given point.
         */
        virtual T DopplerPhase(T Time, T Dist, T RelVel) const;

		/**
         * @brief Gets the frequency in the plane wave at the given point and time.
         * 
         * @param Time - The time at which to compute the frequency at the given point.
         * @param Point - The point in the world frame at which to comptue the frequency.
         * @return T - The frequency at the given point at the given time.
         */
        virtual T Frequency(T Time, const Eigen::Matrix<T, V, 1>& Point) const;

        /**
         * @brief Gets the electric field vector of the plane wave at the given point and time.
         * 
         * @param Time - The time at which to compute the electric field vector at the given point.
         * @param Point - The point in the world frame at which to compute the electric field vector. 
         * @return Eigen::Matrix<std::complex<T>, V, 1> - The electric field vector at the given point and time.
         */
        virtual Eigen::Matrix<std::complex<T>, V, 1> ElectricField(T Time, const Eigen::Matrix<T, V, 1>& Point) const;

        /**
         * @brief Gets the magnetic field vector of the plane wave at the given point and time.
         * 
         * @param Time - The time at which to compute the magnetic field vector at the given point.
         * @param Point - The point in the world frame at which to compute the magnetic field vector.
         * @return Eigen::Matrix<std::complex<T>, V, 1> - The magnetic field vector at the given point and time.
         */
        virtual Eigen::Matrix<std::complex<T>, V, 1> MagneticField(T Time, const Eigen::Matrix<T, V, 1>& Point) const;
    };
}
