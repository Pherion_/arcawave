
#pragma once

// STD includes
#include <complex>

// Eigen includes (ignoring warnings)
#ifdef __clang__
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Weverything"
#elif __GNUG__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wall"
#endif
#include <Eigen/Geometry>
#ifdef __clang__
    #pragma clang diagnostic pop
#elif __GNUG__
    #pragma GCC diagnostic pop
#endif

#ifdef __CUDACC__
#define CUDA_HOSTDEV __device__ __host__
#else
#define CUDA_HOSTDEV
#endif

namespace ArcaWave::Radar
{
    /**
     * @brief Represents an arbitrary source for an electromagnetic radiation field with some source and frequency. This can also be used to represent frequency-moduleated,
     * continuous wave (FMCW) radiation fields by giving the radiation field a non-zero frequency slope.
     * 
     * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
     * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
     */
    template<typename T, uint_fast32_t V>
    class RadiationField
    {

    // Public fields
    public:

        /**
         * @brief The polarization of the radiation field.
         */
        Eigen::Matrix<std::complex<T>, 2, 1> Polarization;

        /**
         * @brief The source in the world frame of the radiation field.
         */
        Eigen::Matrix<T, V, 1> Source;

        /**
         * @brief The initial frequency of the radiation field. Specifcally, this is the frequency at the source of the radiation field at time zero.
         */
        T Freq;

        /**
         * @brief The frequency slope of the radiation field, which represents how the frequency grows with time. The radiation wave can be considered FMCW is this
         * value is non-zero.
         */
        T FreqSlope;

    // Public constructor and deconstructor
    public:

        /**
         * @brief Default constructor of RadiationField.
         */
        RadiationField()
        {}

        /**
         * @brief Creates an arbitrary radiation field from the given polarization, source, and frequency. A frequency slope can also be given to make the radiation field
         * frequency modules continuous wave (FMCW).
         * 
         * @param Polarization - The polarization of the radiation field.
         * @param Source - The source of the radiation field in the world frame.
         * @param Freq - The initial frequency of the radiation field.
         * @param FreqSlope - The frequency slope of the radiation field, which defaults to a value of zero.
         */
        CUDA_HOSTDEV RadiationField(const Eigen::Matrix<std::complex<T>, 2, 1>& Polarization, const Eigen::Matrix<T, V, 1>& Source, T Freq, T FreqSlope = static_cast<T>(0)) :
            Polarization(Polarization), Source(Source), Freq(Freq), FreqSlope(FreqSlope)
        {}

        /**
         * @brief Copy constructor for the radiation field, copying the values of the given radiation field into this new one.
         * 
         * @param Other - The other radiation field to copy the values from.
         */
        RadiationField(const RadiationField<T, V>& Other) :
            Polarization(Other.Polarization), Source(Other.Source), Freq(Other.Freq), FreqSlope(Other.FreqSlope)
        {}

        /**
         * @brief Destroys the radiation field.
         */
        CUDA_HOSTDEV virtual ~RadiationField()
        {}

    // Public operators
    public:

        /**
         * @brief Assignment operator for a radiation field, which copies another radition fields values into this radiation field.
         * 
         * @param Other - The other radiation field to copy.
         * @return RadiationField& - A reference to this radiation field that was copied into.
         */
        RadiationField<T, V>& operator=(const RadiationField<T, V>& Other)
        {
            Polarization = Other.Polarization;
            Source = Other.Source;
            Freq = Other.Freq;
            FreqSlope = Other.FreqSlope;
            return *this;
        }

    // Public abstract methods
    public:

        /**
         * @brief Gets the phase difference between the source of the radiation field and the given point at the given time.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Point - The point in the world frame at which to compute the phase difference from the source of the radiation field.
         * @return T - The phase diference between the the given point and the source.
         */
        virtual T Phase(T Time, const Eigen::Matrix<T, V, 1>& Point) const = 0;

        /**
         * @brief Gets the phase difference given distance.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Dist - The distance between source and the point in the world frame.
         * @return T - The phase difference between the source and the given point.
         */
		CUDA_HOSTDEV virtual T Phase(T Time, T Dist) const = 0;

        /**
         * @brief Gets the phase difference given distance, account for doppler shift.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Dist - The distance between source and the point in the world frame.
         * @param RelVel - The relative velocity between source and given point.
         * @return T - The phase difference between the source and the given point.
         */
        virtual T DopplerPhase(T Time, T Dist, T RelVel) const = 0;

        /**
         * @brief Gets the frequency in the radiation field at the given point and time.
         * 
         * @param Time - The time at which to compute the frequency at the given point.
         * @param Point - The point in the world frame at which to comptue the frequency.
         * @return T - The frequency at the given point at the given time.
         */
        virtual T Frequency(T Time, const Eigen::Matrix<T, V, 1>& Point) const = 0;

        /**
         * @brief Gets the electric field vector of the radiation field at the given point and time.
         * 
         * @param Time - The time at which to compute the electric field vector at the given point.
         * @param Point - The point in the world frame at which to compute the electric field vector. 
         * @return Eigen::Matrix<std::complex<T>, V, 1> - The electric field vector at the given point and time.
         */
        virtual Eigen::Matrix<std::complex<T>, V, 1> ElectricField(T Time, const Eigen::Matrix<T, V, 1>& Point) const = 0;

        /**
         * @brief Gets the magnetic field vector of the radiation field at the given point and time.
         * 
         * @param Time - The time at which to compute the magnetic field vector at the given point.
         * @param Point - The point in the world frame at which to compute the magnetic field vector.
         * @return Eigen::Matrix<std::complex<T>, V, 1> - The magnetic field vector at the given point and time.
         */
        virtual Eigen::Matrix<std::complex<T>, V, 1> MagneticField(T Time, const Eigen::Matrix<T, V, 1>& Point) const = 0;

        /**
         * @brief Gets the gain in a specific direction.
         * 
         * @param Time - The time at which to compute the magnetic field vector at the given point.
         * @param Propogation - The direction to emit waves.
         * @return T - The antenna gain at the given direction.
         */
        virtual T Gain(const Eigen::Matrix<T, V, 1>& Propogation) const
        {
            return 1;
        };
    };
}