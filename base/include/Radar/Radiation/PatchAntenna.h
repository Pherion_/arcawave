
#pragma once

// ArcaWave includes
#include "Radar/Radiation/RadiationField.h"

namespace ArcaWave::Radar
{
    /**
     * @brief Represents an electromagnetic radiation field that propogates from some source with the radiation pattern of patch antenna.
     * This can also be used to represent a frequency-moduleated, continuous wave (FMCW) by giving it a non-zero frequency slope.
     * 
     * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
     * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
     */
    template<typename T, uint_fast32_t V>
    class PatchAntenna : public RadiationField<T, V>
    {
    // Public constructors and deconstructor
    public:

        /**
         * @brief Creates the radiation pattern from the propogation vector, polarization, source, frequency, and frequency slope. This can also be used to represent a
         * frequency modulated, continuous wave (FMCW) radiation pattern if a non-zero frequency slope is given.
         * 
         * @param Propogation - The direction of propogation for the radiation pattern.
         * @param Polarization - The polarization of the radiation pattern.
         * @param Source - The source of the radiation pattern, which is the location of transmitter.
         * @param Freq - The frequency at the transmitter at time zero. Is the constant frequency of the entire radiation pattern if frequency slope is zero.
         * @param FreqSlope - The frequency slope of the radiation pattern, which defaults to a value of zero.
         */
        PatchAntenna
        (
            const Eigen::Matrix<std::complex<T>, 2, 1>& Polarization,
            const Eigen::Matrix<T, V, 1>& Source,
            T Freq,
            T FreqSlope = static_cast<T>(0)
        );

        /**
         * @brief Destroys the patch antenna.
         */
        virtual ~PatchAntenna();

    // Public methods
    public:

        /**
         * @brief Gets the phase difference between the given point and the transmitter at the given time.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Point - The point in the world frame at which to compute the phase difference from the transmitter.
         * @return T - The phase difference between the source and the given point at the given time.
         */
        virtual T Phase(T Time, const Eigen::Matrix<T, V, 1>& Point) const;

        /**
         * @brief Gets the phase difference given distance.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Dist - The distance between source and the point in the world frame.
         * @return T - The phase difference between the source and the given point.
         */
        virtual T Phase(T Time, T Dist) const;

        /**
         * @brief Gets the phase difference given distance, account for doppler shift.
         * 
         * @param Time - The time at which to compute the phase difference between the source and given point.
         * @param Dist - The distance between source and the point in the world frame.
         * @param RelVel - The relative velocity between source and given point.
         * @return T - The phase difference between the source and the given point.
         */
        virtual T DopplerPhase(T Time, T Dist, T RelVel) const;

		/**
         * @brief Gets the frequency in the radiation pattern at the given point and time.
         * 
         * @param Time - The time at which to compute the frequency at the given point.
         * @param Point - The point in the world frame at which to comptue the frequency.
         * @return T - The frequency at the given point at the given time.
         */
        virtual T Frequency(T Time, const Eigen::Matrix<T, V, 1>& Point) const;

        /**
         * @brief Gets the electric field vector of the radiation pattern at the given point and time.
         * 
         * @param Time - The time at which to compute the electric field vector at the given point.
         * @param Point - The point in the world frame at which to compute the electric field vector. 
         * @return Eigen::Matrix<std::complex<T>, V, 1> - The electric field vector at the given point and time.
         */
        virtual Eigen::Matrix<std::complex<T>, V, 1> ElectricField(T Time, const Eigen::Matrix<T, V, 1>& Point) const;

        /**
         * @brief Gets the magnetic field vector of the radiation pattern at the given point and time.
         * 
         * @param Time - The time at which to compute the magnetic field vector at the given point.
         * @param Point - The point in the world frame at which to compute the magnetic field vector.
         * @return Eigen::Matrix<std::complex<T>, V, 1> - The magnetic field vector at the given point and time.
         */
        virtual Eigen::Matrix<std::complex<T>, V, 1> MagneticField(T Time, const Eigen::Matrix<T, V, 1>& Point) const;

        /**
         * @brief Gets the gain in a specific direction.
         * 
         * @param Time - The time at which to compute the magnetic field vector at the given point.
         * @param Propogation - The direction to emit waves.
         * @return T - The antenna gain at the given direction.
         */
        virtual T Gain(const Eigen::Matrix<T, V, 1>& Propogation) const;
    };
}
