
#pragma once

// STL includes
#include <string>
#include <vector>

namespace ArcaWave::Utility
{
    /**
     * @brief A color with red, green, blue, and alpha values.
     * 
     * @tparam T - The type to use for this object. Note, the meaning of a value may drastically change depending on this type. For example, if an integer type is used,
     * the range of a value may exist as [0, 255], whereas if a floating-point value is used, the range of a value may exist as [0.0, 1.0]. This meaning if fully dependent
     * on how it is used.
     */
    template<typename T>
    struct Color
    {
    
    // Public fields
    public:

        /**
         * @brief The red component of the color. Value meaning depends on type used.
         */
        T R;

        /**
         * @brief The green component of the color. Value meaning depends on type used.
         */
        T G;

        /**
         * @brief The blue component of the color. Value meaning depends on type used.
         */
        T B;

        /**
         * @brief The alpha component of the color. Value meaning depends on type used.
         */
        T A;
    };

    /**
     * @brief A color map, which maps a range of values to some color. The mapped range is dependent on which type is used for the color map, and will use the range
     * [0.0, 1.0] for floating-point types such as float or double.
     * 
     * @tparam T - Should be some floating-point type such as float or double. 
     */
    template<typename T>
    class Colormap
    {

    // Private fields
    private:

        /**
         * @brief The list of colors that are mapped to. The list starts with the lowest value colors and rises to the highest value colors.
         */
        const std::vector<Color<T>> Colors;

    // Public constructors/deconstructor
    public:

        /**
         * @brief Creates a new color map from the given list of colors. Note, the color map will interpolate these colors uniformly across the range associated with the type
         * used by the color map.
         * 
         * @param Colors - The list of colors to create the color map from.
         */
        Colormap(const std::vector<Color<T>>& Colors);

        /**
         * @brief Destroys the color map.
         */
        ~Colormap();

    // Public methods
    public:

        /**
         * @brief Gets the color mapped from the given value. Note, any values below outside the mapping range defined by the type are floored and ceilinged into that range.
         * 
         * @param Value - The value to map a color to.
         * @return Color - The color mapped to the given value.
         */
        Color<T> Map(T Value);

    // Public static fields
    public:

        /**
         * @brief Imports a color map from the given file. The files supported are only custom format .cmap file; however, this format is quite
         * simple. The files are stored in plain text, where the first line is an integer denoting how many colors are in the color map, and all
         * the following lines contain four floating-point values, each denoting the red, green, blue, and alpha components of a color respectively.
         * These floating-point values are seperated by a single space between them, and only one color is stored per line. There should be as many
         * colors as specified by the integer in the first line. Note, the first colors are the ones mapped to the lowest values closest to zero, 
         * and the later colors are mapped higher closer to one.
         * 
         * @param Filename - The name of the file to import the color map from.
         * @return Colormap - The color map defined by the given file.
         */
        static Colormap<T> Import(const std::string& Filename);
    };
}
