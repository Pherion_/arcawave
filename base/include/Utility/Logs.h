#pragma once

#include <string>
#include <vector>
#include <experimental/source_location>
#include <nlohmann/json.hpp>
#include <filesystem>


#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/rotating_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/stdout_sinks.h"
#include "spdlog/async.h"
#include "spdlog/sinks/dist_sink.h"



namespace ArcaWave::Utility {

class Logs{
    public:

        Logs();

        Logs(int thrd_num);

        void updateStatus(float cur);

};

        // void Error(std::string msg, const std::experimental::source_location &location = std::experimental::source_location::current());

        void Error(std::exception &e, const std::experimental::source_location &location = std::experimental::source_location::current());

        void check(bool equal, std::string msg, int level = 0, const std::experimental::source_location &location = std::experimental::source_location::current());

        void check(nlohmann::json input);


}