#pragma once

// STL includes
#include <memory>
#include <string>
#include <vector>

// ArcaWave includes
#include "Geometry/Mesh.h"

namespace ArcaWave
{
    namespace Geometry {
        /**
         * @brief The supported file formats for exporting models.
         */
        enum ExportFileType {
            /**
             * @brief The COLLADA format created by the Khronos Group (.dae).
             */
            EX_COLLADA = 0,

            /**
             * @brief The DirectX 2.0 X file format (.x).
             */
            EX_X = 1,

            /**
             * @brief The STEP-File format described by ISO 10303-21 (.step, .stp, .p21)
             */
            EX_STEP = 2,

            /**
             * @brief The wavefront OBJ format with material data if present (.obj, .mtl).
             */
            EX_OBJ = 3,

            /**
             * @brief The wavefront OBJ format without material data (.obj).
             */
            EX_OBJ_NO_MATERIAL = 4,

            /**
             * @brief The polygon file ASCII format described by the Stanford Polygon Library (.ply).
             */
            EX_PLY_ASCII = 7,

            /**
             * @brief The polygon file binary format described by the Stanford Polygon Library (.ply).
             */
            EX_PLY_BINARY = 8,

            /**
             * @brief The 3DS file format used by Autodesk 3ds Max software (.3ds)
             */
            EX_3DS = 9,

            /**
             * @brief Version 2.0 of the glTF (graphics library transmission format) format described by the Khronos Group (.gltf).
             */
            EX_GLTF_V2 = 10,

            /**
             * @brief Version 2.0 of the glTF (graphics library transmission format) binary format described by the Khronos Group (.glb).
             */
            EX_GLTF_V2_BINARY = 11,

            /**
             * @brief The glTF (graphics library transmission format) format described by the Khronos Group (.gltf).
             */
            EX_GLTF = 12,

            /**
             * @brief The glTF (graphics library transmission format) binary format described by the Khronos Group (.glb).
             */
            EX_GLTF_BINARY = 13,

            /**
             * @brief The Assimp binary file format.
             */
            EX_ASSIMP_BINARY = 14,

            /**
             * @brief The Assimp XML file format.
             */
            EX_ASSIMP_XML = 15,

            /**
             * @brief The extensible 3D graphics format (.x3d, .x3dv, .x3db, .x3dz, .x3dbz, .x3dvz).
             */
            EX_X3D = 16,

            /**
             * @brief Autodesk FBX binary format (.fbx).
             */
            EX_FBX_BINARY = 17,

            /**
             * @brief Autodesk FBX ASCII format (.fbx).
             */
            EX_FBX_ASCII = 18,

            /**
             * @brief The 3D manufacturing format described by the 3MF Consortium (.3mf).
             */
            EX_3MF = 19,

            /**
             * @brief Assimp JSON format (.json)
             */
            EX_JSON = 20
        };

        /**
         * @brief A structure for organizing the information needed in the creation of an object.
         *
         * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
         * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
         */
        template<typename T, uint_fast32_t V>
        struct ObjectCreateInfo {

            // Public fields
        public:

            /**
             * @brief The name to give the object.
             */
            std::string ObjectName;

            /**
             * @brief The number of meshes included in the mesh list of this structure.
             */
            uint_fast32_t MeshCount;

            /**
             * @brief An array of pointers to each mesh that will be included within the object.
             */
            std::shared_ptr<std::shared_ptr<Mesh<T, V>>[]> Meshes;

            /**
             * @brief Bounding box for the entire object
             */
            AABB<T, V> BoundingBox;

            // Public constructor and deconstructor
        public:

            /**
             * @brief Creates the object creation information structure.
             */
            ObjectCreateInfo() {}

            /**
             * @brief Destroys the object creation information structure.
             */
            virtual ~ObjectCreateInfo() {}
        };

        /**
         * @brief An arbitrary object contained within the simulation environment. This structure should be used to represent arbitrary, rigid geometry in the environment that can
         * either be dynamic or static. Once the geometry of the object is defined, the object is immutable and can not be changed.
         *
         * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
         * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
         */
        template<typename T, uint_fast32_t V>
        class Object {

        // Protected fields
        protected:

            /**
             * @brief The name given to the object. This is unchangable for a
             */
            const std::string ObjectName;

            /**
             * @brief The number of meshes contianed within the object.
             */
            const uint_fast32_t MeshCount;

            /**
             * @brief An array of pointers to each mesh of the object.
             */
            const std::shared_ptr<std::shared_ptr<Mesh<T, V>>[]> Meshes;

            /**
             * @brief The axis aligned bounding box for the object
             */
            AABB<T, V> BoundingBox;

        // Public constructors and deconstructor
        public:

            /**
             * @brief Creates the new object using the given structure of information necessary at creation.
             *
             * @param Info - A structure containing the information necessary for the creation of the object.
             */
            Object(const std::shared_ptr<ObjectCreateInfo<T, V>> &Info);

            /**
             * @brief Destroys the object.
             */
            virtual ~Object();

        // Public methods
        public:

            /**
             * @brief Creates a new object from this object that is defined from the given list of submeshes, where each entry in this list refers to each mesh of the object in order.
             * Note, the given list of submeshes should be exactly the same length as the number of meshes contained within the object, and each submesh should be valid relative to
             * each mesh within the object respectively.
             *
             * @param Submeshes - A pointer to this of submeshes to create the new object from.
             * @return std::shared_ptr<Object<T, V>> - The newly created object built from the given list of submeshes and this object.
             */
            virtual std::shared_ptr<Object<T, V>> BuildSubObject(const std::shared_ptr<std::shared_ptr<Submesh>[]> &Submeshes) const;

            /**
             * @brief Gets the number of meshes that the object is comprised of.
             *
             * @return uint_fast32_t - The number of meshes comprised in the object.
             */
            virtual uint_fast32_t GetMeshCount() const;

            /**
             * @brief Gets the mesh of the object at the given index. The value given should not be greater than the number of meshes comprised in the object.
             *
             * @param MeshIndex - The index of the mesh to get in the object. Should not be greater than the number of meshes comprised in the object itself.
             * @return std::shared_ptr<Mesh<T, V>> - The pointer to the mesh at the given index.
             */
            virtual std::shared_ptr<Mesh<T, V>> GetMesh(const uint_fast32_t MeshIndex) const;

            /**
             * @brief Gets the name of the object.
             *
             * @return std::string - The name of the object.
             */
            virtual std::string GetName() const;

            /**
             * @brief
             * @return
             */
            virtual AABB<T, V> GetBoundingBox() const;

            /**
             * @brief
             * @param boundingBox
             */
            virtual void SetBoundingBox(const AABB<T, V> &BBox);

        // Public static methods
        public:

            /**
             * @brief Exports the given object to a file of the given name using the format type.
             *
             * @param Obj - The object to export to the file.
             * @param Filename - The name of the file to create and export the object to.
             * @param Type - The type of file format to export the object as.
             */
            static void Export(const std::shared_ptr<Object<T, V>> &Obj, const std::string &Filename, const ExportFileType Type);

            /**
             * @brief Imports an object from the given filename. This supports most 3D object file types such as Collada (.dae), Wavefront (.obj), Autodesk (.fbx), and more. If you
             * attempt to import an object from an unsupported format, or the file is incorreclty format, this function will throw an error and exit. You can specify to smooth the
             * normals of the faces up to a maximum given angle in degrees. This means that any adjacent faces that make an angle smaller than this will be smoothed, else they will
             * remain sharp edges. Note, this will not do anything if the file already specifies the normals for the mesh.
             *
             * @param Filename - The name of the file to import the object from.
             * @param MaxSmoothingAngle - The maximum angle in degrees at which to smooth the surface faces, if normals are not specified in the given file. Default value is 0, which
             * means no smoothing is applied.
             * @return std::shared_ptr<Object<T, V>> - A pointer to the object that was imported from the given file.
             */
            static std::shared_ptr<Object<T, V>> Import(const std::string &Filename, const T MaxSmoothingAngle = 0.0);

        // Protected static methods
        protected:

            static std::shared_ptr<ObjectCreateInfo<T, V>> BuildObjectCreateInfo(const std::string &Filename, const T MaxSmoothingAngle = 0.0);
        };
    }
}
