#pragma once

// STL includes
#include <vector>

// ArcaWave Includes
#include "Geometry/Triangle.h"

namespace ArcaWave
{
    namespace Geometry {
        template<typename T, uint_fast32_t V>
        struct AABB {
            Eigen::Matrix<T, V, 1> Max;

            Eigen::Matrix<T, V, 1> Min;

            AABB();

            AABB(Eigen::Matrix<T, V, 1> Max, Eigen::Matrix<T, V, 1> Min);

            bool PointInAABB(Eigen::Matrix<T, V, 1> Point);

            uint_fast32_t NumPointsInAABB(const std::vector<Eigen::Matrix<T, V, 1>> &Points);

            bool Intersects(const AABB<T, V> &Other);

            AABB<T, V> Union(const AABB<T, V> &Other);
        };
    }
}