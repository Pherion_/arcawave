
#pragma once

// STL includes
#include <memory>
#include <vector>

// ArcaWave includes
#include "Geometry/Object.h"

using namespace Eigen;

namespace ArcaWave
{
    namespace Geometry {


        template<typename T, uint_fast32_t V>
        struct Pose {

            // Public fields
        public:

            Eigen::Matrix<T, V, 1> Position;
            Eigen::Matrix<T, V, V> Orientation;

            // Public constructors and deconstructor
        public:

            /**
             * @brief Default constructor for the pose, placing the position at the origin and setting the orientation matrix to the identity matrix.
             */
            Pose() :
                    Position(Eigen::Matrix<T, V, 1>::Zero()), Orientation(Eigen::Matrix<T, V, V>::Identity()) {}

            /**
             * @brief Constructor to use with intializaer list;
             * @param Position
             * @param Orientation
             */
            Pose(Eigen::Matrix<T, V, 1> Position, Eigen::Matrix<T, V, V> Orientation) :
                    Position(Position), Orientation(Orientation) {}

            /**
             * @brief Destroys the pose.
             */
            ~Pose() {}

            // Public methods
        public:

            /**
             * @brief Transforms the given point in space relative to the origin of this pose, considering both its position and orientation.
             *
             * @param Point - The point to transform relative to the origin of this pose.
             * @return Eigen::Matrix<T, V, 1> - The transfromed point relative to the origin of this pose.
             */
            inline Eigen::Matrix<T, V, 1> TransformPoint(const Eigen::Matrix<T, V, 1> &Point) const {
                return Orientation * Point + Position;
            }

            /**
             * @brief Transforms the given vector in space relative to the origin of this pose, considering its orientation.
             *
             * @param Vec - The vector to transform relative to the origin of this pose.
             * @return Eigen::Matrix<T, V, 1> - The transformed vector relative to the origin of this pose.
             */
            inline Eigen::Matrix<T, V, 1> TransformVector(const Eigen::Matrix<T, V, 1> &Vec) const {
                return Orientation * Vec;
            }
        };

        template<typename T, uint_fast32_t V>
        struct Velocity {
            Eigen::Matrix<T, V, 1> Translational;
            Eigen::Matrix<T, V, 1> Rotational;

            Velocity() :
                    Translational(Eigen::Matrix<T, V, 1>::Zero()), Rotational(Eigen::Matrix<T, V, 1>::Zero()) {}

            Velocity(Eigen::Matrix<T, V, 1> Translational, Eigen::Matrix<T, V, 1> Rotational) :
                    Translational(Translational), Rotational(Rotational) {}

            ~Velocity() {}
        };

        /**
         * @brief Marker to decide wethere instance is dynamic or static.
         */
        enum InstanceType {
            STATIC = 0,
            DYNAMIC = 1
        };

        /**
         * @brief Instance of an object within an environment
         */
        template<typename T, uint_fast32_t V>
        class Instance {

            // Private fields
        private:

            /**
             * @brief The unique ID of the instance within the environment
             */
            const uint_fast32_t InstanceIndex;

            const uint_fast32_t ObjectIndex;

            const InstanceType Type;

            Pose<T, V> Pos;

            Velocity<T, V> Vel;

            // Public constructors and deconstructor
        public:

            Instance(const uint_fast32_t InstanceIndex, const uint_fast32_t ObjectIndex,
                     const InstanceType Type = InstanceType::STATIC);

            ~Instance();

            // Public methods
        public:

            uint_fast32_t GetInstanceIndex() const;

            uint_fast32_t GetObjectIndex() const;

            InstanceType GetType() const;

            Pose<T, V> GetPose() const;

            Velocity<T, V> GetVelocity() const;

            bool SetPose(const Pose<T, V> &Pos);

            bool SetVelocity(const Velocity<T, V> &Vel);

            bool ApplyVelocity(const T Duration);

            Eigen::Matrix<T, 4, 4> GetTransformation() const;
        };
    }
}
