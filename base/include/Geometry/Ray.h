
#pragma once

// Eigen includes (ignoring warnings)
#ifdef __clang__
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Weverything"
#elif __GNUG__
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wall"
#endif
#include <Eigen/Geometry>
#ifdef __clang__
    #pragma clang diagnostic pop
#elif __GNUG__
    #pragma GCC diagnostic pop
#endif

namespace ArcaWave 
{
    namespace  Geometry
    {
        /**
         * @brief A ray that extends infinitely forward from some origin point in some direction.
         *
         * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
         * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
         */
        template<typename T, uint_fast32_t V>
        struct Ray
        {

        // Public fields
        public:

            /**
             * @brief The origin of the ray.
             */
            Eigen::Matrix<T, V, 1> Origin;

            /**
             * @brief The direction that the ray travels. This vector is normalized to a magnitude of one.
             */
            Eigen::Matrix<T, V, 1> Direction;

        // Public construtor and deconstructor
        public:

            /**
             * @brief Default constructors which creates a ray with the origin at the zero vector and no direction. Note this constructor should only be used as a placeholder, as
             * operations with an invalid direction vector may result in errors.
             */
            Ray() :
                Origin(Eigen::Matrix<T, V, 1>::Zero()), Direction(Eigen::Matrix<T, V, 1>::Zero())
            {}

            /**
             * @brief Construct a new ray object with the given origin and direction. Note the direction is normalized on construction.
             *
             * @param O - The origin of the ray.
             * @param D - The direction that the ray travels (normalized on construction).
             */
            Ray(const Eigen::Matrix<T, V, 1>& O, const Eigen::Matrix<T, V, 1>& D) :
                Origin(O), Direction(D.normalized())
            {}

            /**
             * @brief Destroys the ray.
             */
            ~Ray()
            {}

        // Public methods
        public:

            /**
             * @brief Projects the origin of the ray forward along the direction by some distance.
             *
             * @param Distance - The distance to projected point down the ray.
             * @return Eigen::Matrix<T, V, 1> - The resulting projected point along the ray.
             */
            inline Eigen::Matrix<T, V, 1> Project(T Distance) const
            {
                return Origin + (Direction * Distance);
            }
        };
    }
}
