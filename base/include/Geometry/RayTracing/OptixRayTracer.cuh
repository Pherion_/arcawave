
#pragma once

// LIB includes
#include <optix.h>
#include <cuda/api_wrappers.hpp>

// ArcaWave includes
#include "Geometry/RayTracing/RayTracer.h"
#include "Radar/StochasticVCM.cuh"
#include "Radar/DeviceRadarObject.cuh"
#include "Radar/RadarObject.h"

namespace ArcaWave
{
    namespace Geometry
    {
        template <typename T, uint_fast32_t V>
        struct LaunchParameters
        {
            uint32_t NumRaysX;
            uint32_t NumRaysY;
            Eigen::Matrix<T, V, 1> TXPosition;
            Eigen::Matrix<T, V, V> TXOrientation;
            OptixTraversableHandle Handle;
            ProjectedRay<T, V>* PerRayData;
        };

        enum RayInformation{ TX_RAY_TYPE=0, RX_RAY_TYPE, RAY_TYPE_COUNT };

        /*! SBT record for a raygen program */
        struct __align__( OPTIX_SBT_RECORD_ALIGNMENT ) RaygenRecord
        {
            __align__( OPTIX_SBT_RECORD_ALIGNMENT ) char header[OPTIX_SBT_RECORD_HEADER_SIZE];
            void *data;
        };

        /*! SBT record for a miss program */
        struct __align__( OPTIX_SBT_RECORD_ALIGNMENT ) MissRecord
        {
            __align__( OPTIX_SBT_RECORD_ALIGNMENT ) char header[OPTIX_SBT_RECORD_HEADER_SIZE];
            void *data;
        };

        /*! SBT record for a hitgroup program */
        template <typename T, uint_fast32_t V>
        struct __align__( OPTIX_SBT_RECORD_ALIGNMENT ) HitgroupRecord
        {
            __align__( OPTIX_SBT_RECORD_ALIGNMENT ) char header[OPTIX_SBT_RECORD_HEADER_SIZE];
            ArcaWave::Radar::DeviceRadarObject<T, V> RadObj;
        };

        template<typename T, uint_fast32_t V>
        class OptixRayTracer : public RayTracer<T, V>
        {

        // Public constructor and deconstructor
        public:
			explicit OptixRayTracer(const std::shared_ptr<Environment<T, V>>& env);

			~OptixRayTracer();

        // Public methods
        public:

            virtual std::shared_ptr<IlluminatedInstance<T, V>> IlluminateInstance(const std::shared_ptr<IlluminateInstanceInfo<T, V>>& Info) const;

            virtual std::shared_ptr<IlluminatedEnvironment<T, V>> IlluminateEnvironment(const std::shared_ptr<IlluminateEnvironmentInfo<T, V>>& Info) const;

            virtual ProjectedRay<T, V> ProjectRay(const std::shared_ptr<ProjectRayInfo<T, V>>& Info) const;

			virtual std::vector<ProjectedRay<T, V>> ProjectRays(const LaunchInfo<T, V>& Info);

			ProjectedRay<T, V>* ProjectRaysDevice(const LaunchInfo<T, V>& Info);

			ArcaWave::Radar::DeviceRadarObject<T, V> LoadRadarObject(std::shared_ptr<ArcaWave::Radar::RadarObject<T, V>> Obj);

        private:
            void LoadEnvironment();
            void LoadGAS(std::shared_ptr<Object<T, V>> Obj);
            void LoadIAS(std::shared_ptr<Instance<T, V>> Inst);

            OptixTraversableHandle BuildGASAccel(OptixBuildInput& TriangleInput);
            OptixTraversableHandle BuildIASAccel(OptixBuildOperation BuildOperationType);

            void CreateContext();
            void CreateModule();

            void CreateRaygenProgram();
            void CreateMissPrograms();
            void CreateHitgroupPrograms();
            void CreatePipeline();

            void BuildSBT();

            // Private members
        private:

            cuda::device_t TargetDevice;
            CUcontext CudaContext;
            OptixDeviceContext OptixContext;

            OptixPipeline Pipeline;
            OptixPipelineCompileOptions PipelineCompileOptions;
            OptixPipelineLinkOptions    PipelineLinkOptions;
            OptixModule                 Module;
            OptixModuleCompileOptions   ModuleCompileOptions;

            std::vector<OptixProgramGroup> RaygenPGs;
            std::vector<OptixProgramGroup> MissPGs;
            std::vector<OptixProgramGroup> HitgroupPGs;

            OptixShaderBindingTable sbt = {};

            OptixTraversableHandle TopLevelAccel;
            std::vector<OptixInstance> OptixInstances;
            std::vector<OptixTraversableHandle> GASHandles;
            std::vector<void*> GASOutputBuffers;
            void* IASOutputBuffer;

            OptixShaderBindingTable SBT;

            static constexpr uint_fast32_t LOG_NUM = 2048;
        };
    }
}
