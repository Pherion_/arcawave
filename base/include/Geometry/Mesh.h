#pragma once

// STL includes
#include <memory>
#include <vector>

// ArcaWave includes
#include "Geometry/Ray.h"
#include "Geometry/Triangle.h"
#include "Geometry/AABB.h"
#include "Geometry/Material.h"

namespace ArcaWave
{
    namespace Geometry
    {
        /**
         * @brief A subsection of the faces contained within some mesh.
         */
        struct Submesh
        {

        // Private fields
        private:

            /**
             * @brief The indicies of the faces of some mesh that are included in this submesh, sorted in ascending order of value.
             */
            std::vector<uint_fast32_t> Faces;

        // Public constructor & deconstructor
        public:

            /**
             * @brief Creates an initially empty submesh of some other mesh.
             */
            Submesh();

            /**
             * @brief Destroys the submesh.
             */
            ~Submesh();

        // Public methods
        public:

            /**
             * @brief Adds the face index to the submesh, if and only if the face is not already contained within the submesh.
             *
             * @param Face - The face index to add to the submesh.
             */
            void AddFace(uint_fast32_t Face);

            /**
             * @brief Checks whether the submesh contains the given face index.
             *
             * @param Face - The face index to check whether it exists within the submesh.
             * @return true - The face does exist within the submesh.
             * @return false - The face does not exist within the submesh.
             */
            bool ContainsFace(uint_fast32_t Face) const;

            /**
             * @brief Get the number of faces contained in the submesh.
             *
             * @return uint_fast32_t - The number of faces contained in the submesh.
             */
            uint_fast32_t GetFaceCount() const;

            /**
             * @brief Gets the face index of the original mesh at the given index within the submesh. Note this is equivalent to the subscript ([]) operator.
             *
             * @param Index - The index of the face within the submesh.
             * @return uint_fast32_t - The face index relative to the original mesh of the submesh.
             */
            uint_fast32_t GetFace(uint_fast32_t Index) const;

            /**
             * @brief Returns whether the submesh is empty or not.
             *
             * @return true - The submesh is empty, as it contains no faces.
             * @return false - The submesh is not empty, as it containes at least one face.
             */
            bool IsEmpty() const;

            /**
             * @brief Removes the face index from the submesh, if and only if it exists within it submesh already.
             *
             * @param Face - The face index to remove from the submesh.
             */
            void RemoveFace(uint_fast32_t Face);

        // Public operators
        public:

            /**
             * @brief Gets the face index of the original mesh at the given index within the submesh. Note this is equivalent to the GetFace() method.
             *
             * @param Index - The index of the face within the submesh.
             * @return uint_fast32_t - The face index relative to the original mesh of the submesh.
             */
            uint_fast32_t operator[](uint_fast32_t Index) const;

        // Private methods
        private:

            /**
             * @brief Finds the appropriate index of the given face index within the list of face indices, which either represents the index of the face index or the index at
             * which it should be inserted if the face index is not present in the submesh.
             *
             * @param Face - The face index to search for within the list of face indicies of the submesh.
             * @return uint_fast32_t - The index the face index is at or at which it should be inserted if it is not contained within the list.
             */
            uint_fast32_t GetIndex(uint_fast32_t Face) const;
        };

        /**
         * @brief A triangular face of the mesh, which indexes into the list of vertices and normals.
         */
        struct MeshFace
        {
            /**
             * @brief The index of the first vertex of the face, in left-handed form.
             */
            uint32_t A;

            /**
             * @brief The index of the second vertex of the face, in left-handed form.
             */
            uint32_t B;

            /**
             * @brief The index of the third vertex of the face, in left-handed form.
             */
            uint32_t C;
        };

        /**
         * @brief A structure for organizing the information necessary for the creation of a mesh.
         *
         * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
         * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
         */
        template<typename T, uint_fast32_t V>
        struct MeshCreateInfo
        {

        // Public fields
        public:

            /**
             * @brief The number of vertices and normals within the mesh.
             */
            uint_fast32_t VertexCount;

            /**
             * @brief The list of relative vertices that make up the mesh.
             */
            std::shared_ptr<Eigen::Matrix<T, V, 1>[]> Vertices;

            /**
             * @brief The list of normals for each vertex in the mesh.
             */
            std::shared_ptr<Eigen::Matrix<T, V, 1>[]> Normals;

            /**
             * @brief The number of faces contained within the mesh.
             */
            uint_fast32_t FaceCount;

            /**
             * @brief The list of faces that make up the mesh.
             */
            std::shared_ptr<MeshFace[]> Faces;

            /**
             * @brief The material information that makes up the mesh.
             */
            Material<T> MaterialInfo;

            /**
             * @brief The bounding box for the mesh
             */
            AABB<T, V> BoundingBox;

        // Public constructor and deconstructor
        public:

            /**
             * @brief Creates the mesh creation information structure.
             */
            MeshCreateInfo()
            {}

            /**
             * @brief Destroys the mesh creation information structure.
             */
            virtual ~MeshCreateInfo()
            {}
        };

        /**
         * @brief A single mesh used within the environment, made up completely of trainglular faces. It is assumed that any parts of this mesh are fully static to each other and
         * is completely solid.
         *
         * @tparam T - The floating-point type to use for this object. See \ref template "Templatization" for details.
         * @tparam V - The number of entries used in the vector types for the object. See \ref template "Templatization" for details.
         */
        template<typename T, uint_fast32_t V>
        class Mesh
        {

        // Private fields
		public:

            /**
             * @brief The number of vertices and normals within the mesh.
             */
            const uint_fast32_t VertexCount;

            /**
             * @brief The list of relative vertices that make up the mesh.
             */
            const std::shared_ptr<Eigen::Matrix<T, V, 1>[]> Vertices;

            /**
             * @brief The complete list of normals in the mesh relative to the frame of the mesh.
             */
            const std::shared_ptr<Eigen::Matrix<T, V, 1>[]> Normals;

            /**
             * @brief The number of faces contained within the mesh.
             */
            const uint_fast32_t FaceCount;

            /**
             * @brief The list of faces that make up the mesh.
             */
            const std::shared_ptr<MeshFace[]> Faces;

            /**
             * @brief The material of the mesh.
             */
            Material<T> MaterialInfo;

            /**
             * @brief Axis Aligned bounding box for this mesh.
             */
            AABB<T, V> BoundingBox;

		// Public constructors
		public:

			/**
             * @brief Creates the mesh from the given structure of the necessary creation information.
             */
            Mesh(const std::shared_ptr<MeshCreateInfo<T, V>>& Info);

            /**
             * @brief Destroys the mesh.
             */
            virtual ~Mesh();

        // Public methods
        public:

            /**
             * @brief Gets the number of vertices and normals that are contained within the mesh.
             *
             * @return uint_fast32_t - The number of vertices and normals contained within the mesh.
             */
            virtual uint_fast32_t GetVertexCount() const;

            /**
             * @brief Gets the vertex at the given index in the mesh.
             *
             * @param VertexIndex - The index of the vertex to retrieve.
             * @return Matrix<T, V, 1> - The vertex position at the given index.
             */
            virtual Eigen::Matrix<T, V, 1> GetVertex(const uint_fast32_t VertexIndex) const;

            /**
             * @brief Gets the normal of the vertex at the given index in the mesh.
             *
             * @param VertexIndex - The index of vertex the normal is tied to.
             * @return Matrix<T, V, 1> - The normal of the vertex at the given index.
             */
            virtual Eigen::Matrix<T, V, 1> GetNormal(const uint_fast32_t VertexIndex) const;

            /**
             * @brief Gets the number of faces that are contained within the mesh.
             *
             * @return uint_fast32_t - The number of faces contained within the mesh.
             */
            virtual uint_fast32_t GetFaceCount() const;

            /**
             * @brief Gets a face at the given index which gives the indicies of the vertices that make up the face in the mesh.
             *
             * @param FaceIndex - The index of the face in the mesh to acquire.
             * @return MeshFace - The mesh face which containes the indicies to the vertices.
             */
            virtual MeshFace GetFace(const uint_fast32_t FaceIndex) const;

            /**
             * @brief Gets an explicit trianglular face of the given mesh at the given index of the face in the mesh.
             *
             * @param FaceIndex - The index of the face in the mesh to acquire.
             * @return Triangle<T, V> - The explicit triangle that describes the face of the mesh.
             */
            virtual Triangle<T, V> GetTriangle(const uint_fast32_t FaceIndex) const;

            /**
             * @brief Gets the full submesh which represents the full set of faces in the mesh.
             *
             * @return std::shared_ptr<Submesh> - The pointer to full submesh representing this entire mesh.
             */
            virtual std::shared_ptr<Submesh> GetFullSubmesh() const;

            /**
             * @brief Instantiates a seperate mesh from this mesh using the faces specified within the given submesh.
             *
             * @param Sub - The submesh which represents the subset of faces to include in the new mesh.
             * @return std::shared_ptr<Mesh<T, V>> - The newly instantiated mesh which includes all the faces of the original mesh that were specified in the given submesh.
             */
            virtual std::shared_ptr<Mesh<T, V>> BuildSubmesh(const std::shared_ptr<Submesh>& Sub) const;

            AABB<T, V> GetBoundingBox() const;

        	void SetBoundingBox(const AABB<T, V> & BBox);

            Geometry::Material<T> GetMaterial() const;
        };
    }
}
