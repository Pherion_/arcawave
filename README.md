# ArcaWave
### Version 0.2.0
[![pipeline status](https://gitlab.com/digitalgemini/arcawave/badges/master/pipeline.svg)](https://gitlab.com/digitalgemini/arcawave/-/commits/master)

### Dependencies

For this project to work it uses:
- [Assimp v5.0.1](http://www.assimp.org/)
- [Catch v2.11.1](https://github.com/catchorg/Catch2)
- [Cuda API Wrappers](https://github.com/eyalroz/cuda-api-wrappers.git)
- [Eigen v3.3.7](https://gitlab.com/libeigen/eigen)
- [JSON for Modern C++ v3.9.1](https://github.com/nlohmann/json.git)

### Setting up the Development Environment

To compile and link ArcaWave, we use CMake with Clang on LLVM. Though there are many ways to setup this project and have it compile correctly, and you are welcome to explore these different environments, we will share the steps for setting up the environments that we used to create the project.

#### Windows

1. Download and install the correct [CMake binary distribution](https://cmake.org/download/). You need at least version 3.18.0 to compile correctly. While installing, you will be prompted whether or not you would like to add CMake to your system path. Agree, and finish installing.

2. Download and install [LLVM](https://releases.llvm.org/download.html). You should use at least version 9.0.0. While installing, you will be prompted whether or not you would like to add LLVM to your system path. Agree, and finish installing.

3. Download and install both the [CUDA toolkit](https://developer.nvidia.com/cuda-toolkit) and the [OptiX SDK](https://developer.nvidia.com/designworks/optix/download). You should have at least version 11.0 of the CUDA toolkit and version 7.0.0 of the OptiX SDK.

4. Download and install [Visual Studio Community Edition](https://visualstudio.microsoft.com/downloads/). You are welcome to use this as your IDE, but we did not (see step 4). Instead, use the installer to add the Clang compiler for MSVC, the Clang build tools, the CMake tools for Windows, and the Windows SDK. Install all of these, then restart your machine.

5. Download and install [Visual Studio Code](https://code.visualstudio.com/) as your IDE. Two extensions in Visual Studio Code that made compilation extremely easy were the [CMake](https://github.com/twxs/vs.language.cmake) and the [CMake Tools](https://github.com/microsoft/vscode-cmake-tools). When using these, make sure to select the most up-to-date Clang compiler for MSVC at the bottom left side of the Visual Studio Code window. Though they are not required, we found the following plugins to be extremely helpful:
    - [C/C++](https://github.com/microsoft/vscode-cpptools). Note, to make sure that the intellisense of this extension is working, you may have to verify that your C/C++ configuration (found in *.vscode/c_cpp_properties.json*) is setup properly.
    - [Doxygen Documentation Generator](https://github.com/christophschlosser/doxdocgen).

6. Clone the [ArcaWave](https://gitlab.com/digitalgemini/arcawave) git repository into the directory you wish to use it from. Once cloned, you will have to initialize the dependencies for the project. To do this, using a terminal with git installed navigate the directory of the repository and use the commands:

    ```shell
    git submodule init
    git submodule update
    ```

7. To build the project, all you have to do is simply build the appropriate files in the CMake tab, found on the left hand side of the VS Code window. Then once the project has built successfully, you can move to *bin/* folder under the root of the workspace and execute the project.

#### Linux

1. Download and install CMake by moving to the directory in which you want to place the CMake binaries and running the following commands:

    ```shell
    wget https://cmake.org/files/v3.18/cmake-3.18.1-Linux-x86_64.tar.gz
    tar xf cmake-3.18.1-Linux-x86_64.tar.gz
    export PATH="`pwd`/cmake-3.18.1-Linux-x86_64/bin:$PATH"
    ```

    Note this will acquire the binaries needed for version 3.18.1 of CMake on an x86_64 system of Linux. If you prefer a different binary distribution, edit the link used accordingly. You do need at least version 3.18.0 of CMake to compile correctly. You may also have to add the location of the CMake binaries as part of your *.bashrc* file.

2. Download and install LLVM by running the following commands:

    ```shell
    wget https://apt.llvm.org/llvm.sh
    chmod +x llvm.sh
    sudo ./llvm.sh 9
    ```

    Note this will acquire a shell script in the current directory that when ran will download and install the LLVM version given, or in this case version 9.0.0 of LLVM. You should have at least version 9.0.0 of LLVM installed to compile. It is also preferred that you set Clang to be your compiler, which can be done with the following commands:

    ```shell
    export CC=/usr/bin/clang
    export CXX=/usr/bin/clang++
    ```

    You may have to add these two commands as part of your *.bashrc* file to keep these preferences.
    
3. Download and install gcc9 by running the following commands:

    ```shell
    sudo add-apt-repository ppa:ubuntu-toolchain-r/test
    sudo apt update
    sudo apt install gcc-9
    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 60 --slave /usr/bin/g++ g++ /usr/bin/g++-9
    ```

4. Download and install both the [CUDA toolkit](https://developer.nvidia.com/cuda-toolkit) and the [OptiX SDK](https://developer.nvidia.com/designworks/optix/download). You should have at least version 11.0 of the CUDA toolkit and version 7.0.0 of the OptiX SDK.(Follow [this guide](https://ingowald.blog/installing-the-latest-nvidia-driver-cuda-and-optix-on-linux-ubuntu-18-04/#:~:text=Once%20driver%20and%20CUDA%20are,driver,%20CUDA,%20and%20OptiX) if you have Ubuntu 18.04)

5. Install additional OpenGL dependencies: 

    ```shell
    sudo apt-get install libgl1-mesa-dev libglfw3-dev  libxinerama-dev  libxcursor-dev
    ```

6. Clone the [ArcaWave](https://gitlab.com/digitalgemini/arcawave) git repository into the directory you wish to use it from. Once cloned, you will have to initialize the dependencies for the project. To do this, using a terminal with git installed navigate the directory of the repository and use the commands:

    ```shell
    git submodule init
    git submodule update
    ```

7. You should be able to use any IDE that you like on Linux without issue. To build the project, it is prefered that you make a separate directory in the workspace to build within. This can be done with the following commands once you are at the root of the project directory:

    ```shell
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make
    ```
    
    This will build the project and place the executable files in the *bin/* folder under the root of the workspace. Note, if you would like to build in "Debug" mode instead, replace the text "Release" above with "Debug".

### Running the Simulator

Once built, the simulator executable will take two command-line arguments. The first argument will be an input JSON file which describes the scene and scenario to simulate. The second argument will be the name of the file to output the simulated data to. Note that coordinate system that ArcaWave uses if left-handed with the y-axis representeing the altitude vector, as such:

![image](./docs/figures/left_hand_axis.png)

The fields for the JSON input file are described as such:

- `radar_params`: A JSON object which describes the parameters of the radar.
    - `orientation`: A 9-entry list of floating point values which describe the row major rotation matrix of the radar.
    - `tx_position`: A 3-entry list of floating point values which describe the position vector of the bottom-left corner of the transmiter array. 
    - `tx_width`: The number of horizontal transmitters in the transmitter array.
    - `tx_height`: The number of vertical transmitters in the transmitter array.
    - `rx_position`: A 3-entry list of floating point values which describe the position vector of the bottom-left corner of the receiver array.
    - `rx_width`: The number of horizontal receivers in the receiver array.
    - `rx_height`: The number of vertical receivers in the receiver array.
- `objects`: A list of strings which describe the file loaction of each model for each object within the scene.
- `instances`: A list of JSON objects, each of which describe an individual instance of some object.
    - `object_index`: The index in the `objects` array for which this instance shares the model.
    - `is_dynamic`: Whether the instance can dynamically move (true) or can not (false).
    - `position`: A 3-entry list of floating point values which describe the position vector of the instance.
    - `orientation`: A 9-entry list which describes the floating point values which describes the row-major rotation matrix of the instance.
    - `translational`: A 3-entry list of floating point values which describe the translational velocity vector of the instance.
    - `rotational`: A 3-entry list of floating point values which describe the rotational velocity vector of the instance.
