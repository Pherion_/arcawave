
set(TARGET_NAME tests)

# Groups up all the source files
file(GLOB_RECURSE sources ../src/*.cpp *.cpp ../include/*.h)
list(FILTER sources EXCLUDE REGEX ".*main.cpp$")

# Compies the tests
add_executable(${TARGET_NAME} ${sources})

# Links to the assimp libraries
target_link_libraries(${TARGET_NAME} assimp IrrXML)

# Links to pthread if on unix systems
if(UNIX)
    target_link_libraries(${TARGET_NAME} pthread)
endif()
