// //DUT
// #include "Radar/VectorCurrentMoment.h"
// using namespace ArcaWave::Radar;

// // Catch includes
// #include <catch2/catch.hpp>

// // Templates
// typedef double T;
// const uint_fast32_t V = 3;

// TEST_CASE("Vector Current Moment Tests", "[VectorCurrentMoment]")
// {
// 	// Intialize the the test VCM
// 	T freq = 100;
// 	auto interval = ArcaWave::Utility::Interval<T>(100, 1, 0);
// 	auto polarize = Eigen::Matrix<std::complex<T>, 2, 1>{(1,0), (1,0)};
// 	auto DUT = VectorCurrentMoment<T, V>(freq, polarize, interval, interval, interval, interval);

// 	SECTION("Test Constructor")
// 	{
// 		CHECK(DUT.IncPhiInterval.Size == interval.Size);
// 		CHECK(DUT.IncPhiInterval.Offset == interval.Offset);
// 		CHECK(DUT.IncPhiInterval.Step == interval.Step);

// 		CHECK(DUT.IncThetaInterval.Size == interval.Size);
// 		CHECK(DUT.IncThetaInterval.Offset == interval.Offset);
// 		CHECK(DUT.IncThetaInterval.Step == interval.Step);

// 		CHECK(DUT.RefThetaInterval.Size == interval.Size);
// 		CHECK(DUT.RefThetaInterval.Offset == interval.Offset);
// 		CHECK(DUT.RefThetaInterval.Step == interval.Step);

// 		CHECK(DUT.RefPhiInterval.Size == interval.Size);
// 		CHECK(DUT.RefPhiInterval.Offset == interval.Offset);
// 		CHECK(DUT.RefPhiInterval.Step == interval.Step);

// 		CHECK(DUT.Frequency == freq);
// 		CHECK(DUT.Polarization == polarize);

// 		CHECK(DUT.GetMoment(Eigen::Matrix<T, 2, 1> (1, 1), Eigen::Matrix<T, 2, 1> (1, 1)) == Eigen::Matrix<std::complex<T>, V, 1>::Zero());
// 	}

// 	SECTION("Test Setting and Getting Moments")
// 	{
// 		for (uint_fast32_t i = 0; i < 100; i+=2)
// 		{
// 			DUT.SetMoment(Eigen::Matrix<T, 2, 1> (i, i), Eigen::Matrix<T, 2, 1> (i, i), Eigen::Matrix<std::complex<T>, V, 1>(i, i, i));
// 		}

// 		for (uint_fast32_t i = 0; i < 100; ++i)
// 		{
// 			if (i % 2 == 0) CHECK(DUT.GetMoment(Eigen::Matrix<T, 2, 1> (i, i), Eigen::Matrix<T, 2, 1> (i, i)) == Eigen::Matrix<std::complex<T>, V, 1>(i, i, i));
// 			else CHECK(DUT.GetMoment(Eigen::Matrix<T, 2, 1> (i, i), Eigen::Matrix<T, 2, 1> (i, i)) == Eigen::Matrix<std::complex<T>, V, 1>::Zero());
// 		}
// 	}
// }
